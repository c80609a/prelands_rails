# Stage: ruby ##########################################################################################################
FROM ruby:2.7.3-alpine3.13 as ruby
LABEL description="Base ruby image used by other stages"

# Stage runner #########################################################################################################
FROM ruby as runner
LABEL description="Builds an image ready to be run"

# Install build deps and gems from all environments under vendor/bundle path
RUN apk add --update --no-cache \
   build-base git zip sqlite-dev shared-mime-info

RUN addgroup -g 1000 -S app \
 && adduser -u 1000 -S app -G app \
 && mkdir -p /home/app/tmp \
 && chown -R app:app /home/app

USER app
WORKDIR /home/app

# Copy source files according to .dockerignore policy
# Make sure your .dockerignore file is properly configure to ensure proper layer caching
COPY --chown=app:app . /home/app

RUN bundle install --jobs 8 --retry 3

ENTRYPOINT ["bundle", "exec"]
