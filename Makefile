.PHONY: test all

DIR = $(shell pwd)

setup_local_gem:
	bundle config local.prelands_rails $(DIR)

all:
	@docker-compose -f docker-compose.local.yml build

test:
	@docker-compose -f docker-compose.local.yml run runner rspec

build_and_push_docker:
	./build_and_push.sh
