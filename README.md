# PrelandsRails (Rails based gem) 

Код, занимающийся валидацией и публикацией прелендов V4.

# How to publish gem

1. Bump version inside `prelands_rails.gemspec`.
2. Make commit & push to `master` branch.
3. Then checkout `tags` branch and `git rebase master` (accept theirs if there are any conflicts).
4. Then `git push` - and Gitlab CI publish the new version of the gem.

# Local developing

Execute in bash console using the actual path to the gem folder:

    bundle config local.prelands_rails /home/scout/git/prelands_rails/
    
Expected output:
    
    You are replacing the current global value of local.prelands_rails, which is currently "/home/scout/git/preland_rails/"

In the Gemfile of a host application specify actual gem version, branch and semi-github path:

    gem 'prelands_rails', '0.1.4', github: 'prelands_rails/prelands_rails', branch: 'master'
    
Run `bundle`, launch puma server. Now your host application uses the local files.