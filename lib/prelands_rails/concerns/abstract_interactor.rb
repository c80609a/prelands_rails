# frozen_string_literal: true

module PrelandsRails
  module AbstractInteractor
    include ::Interactor
    include ::Interactor::Contracts
    include CanHandleErrors

    delegate :fail!, :to => :context

    def call
      begin
        act
      rescue ::Interactor::Failure # bad promises
        report_error 'Bad promises: %s' % context.errors, context.to_h
        fail! errors: context.errors
      rescue ::ActiveRecord::RecordNotFound => e
        report_error e.message, context.to_h
        fail! errors: 'not found'
      rescue StandardError => e
        error_handler e, context.to_h
        fail! errors: e.message
        custom_error_handler e
      end
    end

    private

    def act
      raise NotImplementedError, "The `act` method must be defined in #{self.class}"
    end

    def custom_error_handler(_e); end

    protected

    def bad_expects(breaches)
      errors = build_errors_message breaches
      fail! errors: errors
    end

    # [{0=>["fail1", "fail2"], 1=>["fail1", "fail3"]}] => "fail1 ; fail2 ; fail3"
    def build_errors_message(breaches)
      msgs = breaches.map(&:messages).inspect.scan /(?<=")[^"\]\[{}]+(?=")/
      (msgs - [', ']).map { |m| [m,nil] }.to_h.keys.join('; ')
    end
  end
end
