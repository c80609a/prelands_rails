# frozen_string_literal: true

module PrelandsRails
  module Base

    private

    RX_HTML_FILE           = /\.html/
    RX_CSS_FILE            = /\.css/.freeze
    NAME_RX                = /index_(\w{2})\.html/
    # DIST_FOLDER            = 'dist'
    # KNOWN_FILES_EXTENSIONS = /\.(gif|jpe?g|tiff?|png|webp|bmp|mov|avi|wmv|flv|3gp|mp4|mpg|css|js|ico|icon)/.freeze

    # извлекаем входящие html файлы из контекста
    def incoming_html_files
      context.files_content.select { |key| key =~ RX_HTML_FILE }
    end

    # извлекаем скомпилированные html файлы из контекста
    def compiled_html_files
      context.compiled_htmls.select { |key| key =~ RX_HTML_FILE }
    end

    # извлекаем входящие css файлы из контекста
    def incoming_css_files
      context.files_content.select { |key| key =~ RX_CSS_FILE }
    end

    def make_tmp_path
      '/tmp/%s' % SecureRandom.uuid
    end
  end
end
