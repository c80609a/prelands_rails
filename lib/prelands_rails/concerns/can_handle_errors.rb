# # frozen_string_literal: true

module PrelandsRails
  module CanHandleErrors
    private

    # Отправит ошибку в Sentry, выведет в лог красным название класса, текст ошибки и backtrace
    def error_handler(error, extra = {})
      error_text = ['[%s] ' % self.class, error.message].join
      Rails.logger.warn "\n\t\e[31m\e[1m%s\e[0m" % error_text
      Rails.logger.warn "\n\e[31m\t%s \e[0m\n" % (error.backtrace[0..9] *  "\n\t")
      Raven.capture_exception(error, extra: extra) if defined?(Raven)
    end

    # Отправит message в Sentry, в dev/test лог скинет pretty extra info
    def report_error(error_message = context.errors, extra = {})
      message = ['[%s] ' % self.class, error_message].join

      if Rails.env.test? || Rails.env.development?
        div = '-' * 120
        Rails.logger.warn div
        Rails.logger.warn "\e[31m\e[1m%s\e[0m\n" % message
        # Rails.logger.ap extra
        Rails.logger.warn div
      else
        Raven.capture_message(message, extra: extra) if defined?(Raven)
      end

    end
  end
end
