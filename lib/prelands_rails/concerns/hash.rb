class Hash
  def to_struct
    Struct.new(*keys).new(*values)
  end

  # Convert Hash to OpenStruct recursively
  # https://coderwall.com/p/74rajw/convert-a-complex-nested-hash-to-an-object
  def to_o
    JSON.parse to_json, object_class: OpenStruct
  end
end
