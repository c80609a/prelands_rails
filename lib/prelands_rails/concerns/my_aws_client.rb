# frozen_string_literal: true

require 'aws-sdk-s3'

module PrelandsRails
  class MyAwsClient

    ACL_PUBLIC_READ = 'public-read'

    def initialize(access_key, secret_key, region)
      @s3 = Aws::S3::Resource.new(region: region, access_key_id: access_key, secret_access_key: secret_key)
    end

    def bucket_objects_count(bucket_name)
      bucket = @s3.bucket bucket_name
      result = bucket.objects.count
      log :bucket_objects_count, 'bucket_name=%s, result=%s' % [bucket_name, result]
      result
    rescue Aws::S3::Errors::NoSuchBucket
      log :bucket_objects_count, ['No such bucket: ', bucket_name].join
    end

    # @return [struct Aws::S3::Types::ListBucketsOutput]
    def list_buckets
      result = @s3.client.list_buckets({})
      log :list_buckets, result.buckets.map(&:name)
    end

    # @return [True]
    def create_bucket_if_not_exists(bucket_name)
      log :create_bucket_if_not_exists, 'bucket_name=%s' % bucket_name
      @s3.create_bucket bucket: bucket_name
      true
    rescue Aws::S3::Errors::BucketAlreadyOwnedByYou
      true
    rescue StandardError => e
      log :create_bucket_if_not_exists, e.message
      false
    end

    def delete_bucket(bucket_name)
      log :delete_bucket, 'bucket_name=%s' % bucket_name
      @s3.client.delete_bucket bucket: bucket_name
    rescue StandardError => e
      log :delete_bucket, e.message
      false
    end

    def list_objects(bucket_name)
      log :list_objects, 'bucket_name=%s' % bucket_name
      result = (@s3.client.list_objects bucket: bucket_name).contents.map(&:key)
      log :list_objects, result
      result
    rescue StandardError => e
      log :list_objects, e.message
      false
    end

    def delete_objects(bucket_name, objects)
      log :delete_objects, 'bucket_name=%s' % bucket_name
      result = @s3.client.delete_objects({
        bucket: bucket_name,
        delete: {
          objects: objects
        }
      })
      log :delete_objects, result
    rescue StandardError => e
      log :delete_objects, e.message
      false
    end

    # @param [String] path_to_file  Абсолютный к файлу на хосте.
    # @param [String] s3_file_name  Под каким именем будет сохранён файл на AWS.
    # @param [String] bucket_name   Название bucket

    def upload_file(path_to_file, s3_file_name, bucket_name)
      begin
        obj = @s3.bucket(bucket_name).object s3_file_name
        begin
          if obj.get
            log :upload_file, [s3_file_name, ' exists, skipping..'].join('')
          end
        rescue Aws::S3::Errors::NoSuchKey => _e
          obj.upload_file path_to_file, acl: ACL_PUBLIC_READ
          log :upload_file, [s3_file_name, ' uploaded to "', bucket_name, '".'].join('')
        end
      rescue Aws::S3::Errors::NoSuchBucket => _e
        log :upload_file, 'No such bucket, create bucket: \'%s\'] ' % bucket_name
        create_bucket_if_not_exists bucket_name
        retry
      end
    end

    # Переместит файл в пределах +bucket_name+
    # @return true если успешно переместил
    def move_file(s3_old_file_name, s3_new_file_name, bucket_name)
      log :move_file, ['', s3_old_file_name, ' moving to ', s3_new_file_name, ' in ', bucket_name, '.'].join('"')

      obj        = Aws::S3::Object.new(bucket_name, s3_old_file_name, client: @s3.client)
      new_target = [bucket_name, s3_new_file_name].join('/')

      begin
        obj.move_to new_target
      rescue Aws::S3::Errors::NoSuchKey
        log :move_file, 'Aws::S3::Errors::NoSuchKey: The specified key does not exist: "%s"' % s3_old_file_name
        return false
      end

      obj = Aws::S3::Object.new(bucket_name, s3_new_file_name, client: @s3.client)

      begin
        obj.acl.put({ acl: ACL_PUBLIC_READ })
      rescue Aws::S3::Errors::NoSuchKey
        log :move_file_acl_put, 'Aws::S3::Errors::NoSuchKey: The specified key does not exist: "%s"' % s3_old_file_name
        return false
      end

      true
    end

    private

    def log(method, output)
      Rails.logger.info "\r\t\e[33m\e[1m<MyAwsClient.%s> %s\e[0m" % [method, output]
      nil
    end
  end
end

# require 'my_aws_client'
# client = MyAwsClient.new ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'], ENV['AWS_S3_REGION']
#
# client.list_buckets
# => #<struct Aws::S3::Types::ListBucketsOutput
#  buckets=
#    #<struct Aws::S3::Types::Bucket name="psckeditor-dev", creation_date=2020-12-24 13:22:29 UTC>,
#  owner=#<struct Aws::S3::Types::Owner display_name=nil, id="ca37dafdbd424d0f11ccb2c87a2152b6174bf3986ebbf22760d77eb306f608c2
#
# client.bucket_objects_count "psckeditor-dev"
# => 3
#
# client.bucket_objects_count "psckeditor-dev222222222"
# => nil
#
# client.create_bucket_if_not_exists 'psckeditor-dev2'
# => true
#
# MyAwsClient.create_bucket_if_not_exists 'psckeditor-dev2'
# => true
