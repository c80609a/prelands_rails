# frozen_string_literal: true

module PrelandsRails
  #
  # Примет zip-файл с исходниками преленда и опубликует этот преленд.
  #
  class CreateSimpleSource
    include ::PrelandsRails::AbstractInteractor
    include ::Interactor::Organizer
    include ::Interactor::Contracts

    expects do
      required(:archive).filled                                                 # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
      required(:preland_domain_ids).filled(type?: Array)
      required(:expected_locales).filled(type?: Array)                          # Список локалей, для которых must were all index_<locale>.html files in the zip archive
      required(:s3_credentials).schema do
        required(:access_key).filled(:str?)
        required(:secret_key).filled(:str?)
        required(:region).filled(:str?)
      end
      required(:bucket_names).filled(type?: Array)
      required(:aws_prefix).value(type?: String)                                # имя директории в букете
      required(:preland_id).filled
      required(:static_js_path).value(type?: String)                            # путь к размещённым в интернете нашим js скриптам
      required(:model_preland_simple_source).filled
    end

    before do
      fail!(errors: 'Preland domains missing') if context.preland_domain_ids.empty?
    end

    assures do
      required(:incoming_locales).filled(type?: Array)
      required(:files_content).value(type?: Hash)                               # распакованные файлы из архива { file_name[String] => file_content[String], ... }
      required(:compiled_htmls).value(type?: Hash)                              # скомпилированные исходники    { file_name[String] => compiled_content[String], ... }
      required(:preland_simple_source).filled
      optional(:warnings).maybe(type?: Array)
    end

    organize DetectIncomingLocales,
             CheckZipFiles,
             ValidateZipContent,
             Compile,
             Upload,
             CreateRecord

    on_breach { |breaches| bad_expects breaches }
  end
end
