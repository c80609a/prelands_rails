# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    #
    # Проверит наличие файлов и директорий в архиве с исходниками преленда:
    #
    #   * должен быть файл index.js
    #   * должен быть файл index.css
    #   * должна быть директория images/
    #   * должны быть все языковые файлы index_<lang>.html, согласно локалям преленда
    #   * размер любого файла не должен превышать 8мб
    #
    class CheckZipFiles
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base
      include DetectAbsentFiles
      include ExtractFiles

      expects do
        required(:archive).filled                                               # Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:expected_locales).value(type?: Array)
        required(:incoming_locales).filled(type?: Array)
      end

      assures do
        required(:files_content).value(type?: Hash)                             # js,html,css: { file_name => file_content, ... }
        required(:incoming_files).value(type?: Array)                         # список всех файлов архива { ftype, name }
      end

      before do
        @expected_files =
          [
            { ftype: :directory, name: 'images/'   },
            { ftype: :file,      name: 'index.css' },
            { ftype: :file,      name: 'index.js'  }
          ]

        # читаем в память [и компилируем] все входящие с исходником локали
        (context.expected_locales + context.incoming_locales).uniq.each do |short_name|
          @expected_files << { ftype: :file, name: 'index_%s.html' % short_name }
        end

        @expected_files = @expected_files.map(&:to_struct).freeze
      end

      def act
        # если нет хотя бы одного файла - уходим с ошибкой
        absents = detect_absent_files @expected_files, incoming_files
        if absents.present?
          fail! errors: absents.join('; ')
        end

        # считываем в память файлы, которые необходимо валидировать
        rx1   = /\.(css|js|html)/
        files = @expected_files.select { |file| file.ftype == :file && file.name =~ rx1 }
        context.files_content, context.tmp_paths = read_into_memory files, context.archive.tempfile

        # фиксируем список всех файлов из архива
        context.incoming_files = incoming_files
      end

      def incoming_files
        @incoming_files ||= Zip::File.open(context.archive.tempfile) do |zipfile|
          zipfile.map do |file|
            if file.size > MAX_SIZE
              fail! errors: 'File too large when extracted (must be less than %s bytes)' % MAX_SIZE
            end
            { ftype: file.ftype, name: file.name }
          end
        end.map(&:to_struct).freeze
      end

      on_breach { |breaches| bad_expects breaches }

      private

      MAX_SIZE = 1024**2 * 8 # 8 MiB
    end
  end
end
