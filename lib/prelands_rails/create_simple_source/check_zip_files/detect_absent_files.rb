# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class CheckZipFiles
      #
      # Вернёт массив вида ['index.js not found',..]
      # или пустой массив, если все ожидаемые файлы присутствуют в архиве с исходниками преленда
      #
      module DetectAbsentFiles
        def detect_absent_files(expected_files, incoming_files)
          raise 'expected_files must be present' unless expected_files.present?

          # опрашиваем по списку пришедший контент
          expected_files.map do |efile|
            sought = incoming_files&.find { |ifile| ifile.ftype == efile.ftype && ifile.name == efile.name }
            if sought.nil?
              '%s not found' % efile.name
            end
          end.compact
        end
      end
    end
  end
end
