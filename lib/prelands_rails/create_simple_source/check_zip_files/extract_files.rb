# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class CheckZipFiles
      module ExtractFiles

        # @return [Hash] { file_name => file_content, ... }
        def read_into_memory(expected_files, tempfile)
          Zip::File.open(tempfile) do |zipfile|
            zipfile.map do |entry|
              next unless expected_files.find_index { |efile| efile.name == entry.name }.present?

              # Extract
              tmp_path = make_tmp_path
              entry.extract tmp_path

              # Read into memory
              content = entry.get_input_stream.read

              # Remove tmp file
              # File.delete(tmp_path) if File.exist?(tmp_path)

              [entry.name, content]
            end.compact.to_h
          end
        end
      end
    end
  end
end
