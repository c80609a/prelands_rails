module PrelandsRails
  class CreateSimpleSource
    #
    # Из валидных исходников соберёт преленд.
    #
    class Compile
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:files_content).value(type?: Hash)                             # { file_name[String] => file_content[String], ... }
        required(:static_js_path).value(type?: String)
      end

      assures do
        required(:compiled_htmls).value(type?: Hash)                            # { file_name[String] => compiled_content[String], ... }
      end

      def act
        context[:compiled_htmls] =
          incoming_html_files.map do |key, content|
            if NAME_RX =~ key
              compiled = HtmlCompiler.new(content, index_css, index_js, $1, context.static_js_path).compiled
              [key, compiled]
            end
          end.to_h
      end

      on_breach { |breaches| bad_expects breaches }

      private

      def index_js
        context.files_content['index.js']
      end

      def index_css
        context.files_content['index.css']
      end
    end
  end
end
