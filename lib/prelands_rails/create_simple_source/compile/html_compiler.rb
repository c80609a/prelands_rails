# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class Compile
      #
      # Компилирует html преленда.
      #
      class HtmlCompiler

        attr_reader :compiled

        # @param [String] string    Исходное содержимое входящего html файла.
        # @param [String] index_css Исходное содержимое файла index.css
        # @param [String] index_js  Исходное содержимое файла index.js
        # @param [String] lang      Локаль
        #
        def initialize(string, index_css, index_js, lang, static_js_path)
          @string         = string.gsub(/(\r\n?|\n)/, '') # убираем переносы, мешающие регуляркам
          @index_css      = index_css
          @index_js       = index_js
          @lang           = lang
          @static_js_path = static_js_path
          prepare
        end

        private

        META = '<meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">'
        HTML = '<!DOCTYPE html><html lang="%{lang}">%{head}%{body}</html>'

        attr_accessor :head, :body

        def prepare
          build_head
          build_body
          build_html
        end

        # Из входящего head выберем только то, что нужно,
        # добавим meta,
        # впишем авторский стиль из index.css,
        # добавим наш скрипт

        def build_head
          # фиксируем head
          head_rx = /(?<=<head).+(?=<\/head>)/
          result  = @string[head_rx]

          # фиксируем title
          title_rx = /<title[^>]*>[^<]+<\/title>/
          title    = @string[title_rx]

          # из head выбираем подключаемые скрипты (js)
          head_script_rx = /<script[^>]*>[^<]*<\/script>/
          head_scripts   = result.scan head_script_rx

          # из head выбираем подключаемые линки (css), исключая `index.css`
          head_link_rx = /<link[^>]+>/
          head_links   = result.scan head_link_rx
          head_links   = head_links.select { |string| !string['index.css'] }.compact

          # собираем извлечённое в один массив
          @head = head_scripts.concat(head_links)
          @head.unshift title
          @head.unshift META
          @head << ('<style>%s</style>' % @index_css)
          @head = '<head>%s</head>' % @head.join('')

        end

        def build_body
          body_rx = /<body[^>]*>.+<\/body>/
          result  = @string[body_rx]
          @body   = result
          @body  += static_js
        end

        # собираем результат
        def build_html
          @compiled =
            HTML % {
              lang: @lang,
              head: @head,
              body: @body
            }
        end

        def static_js
          <<-JS
<script>
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.async = true;
    js.src = "#{@static_js_path}?" + Math.random();
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'sdk'));
</script>
          JS
        end
      end
    end
  end
end
