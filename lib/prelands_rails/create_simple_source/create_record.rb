module PrelandsRails
  class CreateSimpleSource
    #
    # Создаст/обновит запись ::Preland::SimpleSource
    #
    class CreateRecord
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:archive).filled                                               # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:aws_prefix).value(type?: String)                              # имя директории в букете
        required(:preland_id).filled
        required(:preland_domain_ids).value(type?: Array)
        required(:incoming_locales).filled(type?: Array)
        required(:model_preland_simple_source).filled
      end

      assures do
        required(:preland_simple_source).filled
      end

      before do

      end

      def act
        params = {
          aws_prefix:         context.aws_prefix,
          archive:            context.archive,
          preland_id:         context.preland_id,
          preland_domain_ids: context.preland_domain_ids,
          locales:            context.incoming_locales
        }
        context.preland_simple_source =
          context.model_preland_simple_source.create! params
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
