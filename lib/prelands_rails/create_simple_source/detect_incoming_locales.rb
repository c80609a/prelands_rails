# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    #
    # запоминаем, какие локали пришли в исходнике
    #
    class DetectIncomingLocales
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:archive).filled                                               # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
      end

      assures do
        required(:incoming_locales).filled(type?: Array)
      end

      def act
        context.incoming_locales =
          Zip::File.open(context.archive.tempfile) do |zipfile|
            zipfile.map do |file|
              file.name[NAME_RX]
              $1
            end
          end.compact
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
