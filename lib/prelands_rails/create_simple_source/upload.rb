# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    #
    # Загрузит на aws скомпилированные html, icon.ico, index.js и содержимое директории images/
    #
    class Upload
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:archive).filled                                               # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:compiled_htmls).value(type?: Hash)                            # скомпилированные исходники    { file_name[String] => compiled_content[String], ... }
        required(:s3_credentials).schema do
          required(:access_key).filled(:str?)
          required(:secret_key).filled(:str?)
          required(:region).filled(:str?)
        end
        required(:bucket_names).value(type?: Array)
        required(:aws_prefix).value(type?: String)
        required(:files_content).value(type?: Hash)                             # { file_name => file_content, ... }
      end

      assures do

      end

      before do
        @uploader = Uploader.new(context.s3_credentials, context.aws_prefix)
      end

      def act
        context.bucket_names.each do |bucket_name|
          images_files.map do |file_name, file_path|
            @uploader.upload_file file_name, file_path, bucket_name
          end

          compiled_html_files.map do |file_name, content|
            @uploader.upload_content file_name, content, bucket_name
          end

          @uploader.upload_content 'index.js', context.files_content['index.js'], bucket_name
        end

        del_tmp_files
        context.uploaded_images = images_files
      end

      on_breach { |breaches| bad_expects breaches }

      private

      IMAGES = /\A(images\/.+|icon\.ico)\z/

      def images_files
        return @images_files if defined?(@images_files)

        @tmp_paths = []

        @images_files ||= Zip::File.open(context.archive.tempfile) do |zipfile|
          zipfile.map do |entry|
            next if entry.name.index(IMAGES) != 0

            tmp_path = make_tmp_path
            entry.extract tmp_path
            @tmp_paths << tmp_path

            [entry.name, tmp_path]
          end.compact.to_h
        end
      end

      def del_tmp_files
        @tmp_paths.each do |path|
          File.delete(path) if File.exists?(path)
        end
      end
    end
  end
end
