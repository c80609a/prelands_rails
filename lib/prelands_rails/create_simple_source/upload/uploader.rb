# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class Upload
      #
      # Загрузчик файлов на AWS.
      #
      class Uploader
        def initialize(creds, aws_prefix, client_class = ::PrelandsRails::MyAwsClient)
          @folder = aws_prefix
          @client = client_class.new creds[:access_key], creds[:secret_key], creds[:region]
        end

        # На вход подан путь к файлу на хосте, который грузим на aws с указанным именем.
        #
        # @param [String] file_path
        # @param [String] s3_file_name
        # @param [String] bucket_name

        def upload_file(s3_file_name, file_path, bucket_name)
          @client.upload_file file_path, path(s3_file_name), bucket_name
        end

        # На вход подана строка +file_content+. Сохраняем её, как файл с указанным именем.
        #
        # @param [String] s3_file_name
        # @param [String] file_content
        # @param [String] bucket_name

        def upload_content(s3_file_name, file_content, bucket_name)
          Tempfile.create(SecureRandom.uuid) do |file|
            file.write file_content.force_encoding('utf-8')
            file.close
            @client.upload_file file.path, path(s3_file_name), bucket_name
          end
        end

        private

        def path(file)
          [@folder, file].join('/')
        end
      end
    end
  end
end
