# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    #
    # Примет содержимое файлов и валидирует их контент.
    #
    class ValidateZipContent
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor::Organizer
      include ::Interactor::Contracts

      expects do
        required(:files_content).value(type?: Hash)                             # { file_name[String] => file_content[String], ... }
      end

      assures do
        optional(:warnings).maybe(type?: Array)
      end

      organize ValidateIncomingFiles, ValidateHtml, ValidateJs, ValidateCss

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
