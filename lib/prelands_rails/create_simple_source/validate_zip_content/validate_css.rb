# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      #
      # Валидирует все css файлы.
      #
      class ValidateCss
        include ::PrelandsRails::AbstractInteractor
        include ::Interactor
        include ::Interactor::Contracts
        include ::PrelandsRails::Base

        expects do
          required(:files_content).value(type?: Hash) # { file_name => file_content, ... }
        end

        assures do

        end

        def act
          errors =
            incoming_css_files.map do |key, content|
              content = Css.new(content)
              next if content.valid?
              [key, content.errors]
            end.compact.to_h
          fail! errors: errors if errors.any?
        end
      end
    end
  end
end
