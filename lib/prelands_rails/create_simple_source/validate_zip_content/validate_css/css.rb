# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      class ValidateCss
        #
        # Принимает строку с css и валидирует её.
        # Правила, обязательные для всех css файлов.
        #
        class Css
          attr_reader :errors

          CUSTOM_FONTS       = /\.(eot|woff|woff2|ttf)/.freeze
          SVG_LINKS_DETECTED = /\.svg/.freeze
          IMAGES_BAD_PATH    = /\/images/.freeze

          # @param [String] string Содержимое css файла.
          def initialize(string)
            @errors = []
            @string = string
          end

          def valid?
            @errors = [
              check_svg_links,
              check_images_relative_paths,
              check_custom_fonts_absence
            ].compact

            @errors.none?
          end

          private

          # @return [nil]     Если всё ОК
          # @return [String]  Иначе - вернёт сообщение об ошибке

          def check_svg_links
            return unless @string =~ SVG_LINKS_DETECTED

            'Links to svg detected inside css file. Please, use inline variant of svg.'
          end

          def check_images_relative_paths
            return unless @string =~ IMAGES_BAD_PATH

            'Images urls in css files must be relative.'
          end

          def check_custom_fonts_absence
            return unless @string =~ CUSTOM_FONTS

            'Custom fonts detected in css files. Please, use Google fonts.'
          end
        end
      end
    end
  end
end
