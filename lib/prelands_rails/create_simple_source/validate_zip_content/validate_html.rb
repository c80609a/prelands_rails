# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      #
      # Валидирует все html файлы.
      #
      class ValidateHtml
        include ::PrelandsRails::AbstractInteractor
        include ::Interactor
        include ::Interactor::Contracts
        include ::PrelandsRails::Base

        CONTINUE_ID = 'continue'

        expects do
          required(:files_content).value(type?: Hash)                           # { file_name => file_content, ... }
        end

        assures do

        end

        def act
          errors =
            incoming_html_files.map do |key, content|
              content = Html.new(content, CONTINUE_ID)
              next if content.valid?
              [key, content.errors]
            end.compact.to_h

          fail! errors: errors if errors.any?
        end

        on_breach { |breaches| bad_expects breaches }
      end
    end
  end
end
