# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      class ValidateHtml
        #
        # Принимает строку с html разметкой и валидирует её.
        #
        class Html
          attr_reader :errors

          # @param [String] string Содержимое html файла.
          def initialize(string, continue_id)
            @errors      = []
            @continue_id = continue_id
            @string      = string.gsub(/(\r\n?|\n)/, '') # убираем переносы, мешающие регуляркам
          end

          def valid?
            @errors = [
              check_internal_scripts,
              check_internal_styles,
              check_continue_element,
              check_js_plug,
              check_css_plug,
              check_title_presence,
              check_a_hrefs,
              check_favicon
            ].compact

            !@errors.any?
          end

          private

          # @return [nil]     Если внутри html НЕ содержится JavaScript код в виде <script>...</script>
          # @return [String]  Иначе - вернёт сообщение об ошибке
          def check_internal_scripts
            rx  = /<script[^>]*>([^<]+)<\/script>/
            return unless any(rx)

            'A JavaScript code is detected inside html-file. Please, move it to index.js.'
          end

          def check_internal_styles
            rx  = /<style[^>]*>([^<]+)<\/style>/
            return unless any(rx)

            'An internal CSS is detected inside html-file. Please, move it to index.css.'
          end

          def check_continue_element
            rx  = Regexp.new('id="%s"' % @continue_id)
            res = @string.scan(rx).flatten
            return if res.size == 1

            return 'The continue element with id="%s" must be uniq. Found %s matches.' % [@continue_id, res.size] if res.size > 1

            'Not found continue element with id="%s"' % @continue_id
          end

          def check_js_plug
            body_rx  = /<body[^>]*>(.+)<\/body>/
            js_rx    = '<script src="index.js"'

            @string[body_rx]
            res = $1&.[](js_rx)
            return if res.present?

            'Plug index.js script before closing tag of body.'
          end

          def check_css_plug
            head_rx  = /(?<=<head)(.+)(?=<\/head>)/
            css_rx   = 'href="index.css"'

            @string[head_rx]
            res = $1&.[](css_rx)
            return if res.present?

            'Plug index.css before closing tag of head.'
          end

          def check_title_presence
            title_rx = /<title>[^<]+<\/title>/
            return if @string[title_rx]

            'Tag `title` not found.'
          end

          def check_a_hrefs
            ahref_rx = /<a[^>]+href="[^"]+"/
            return unless @string[ahref_rx]

            'Links with href attribute found.'
          end

          def check_favicon
            favicon_rx = /<link.+href="icon.ico">/
            return if @string[favicon_rx]

            '`link href="icon.ico"` not found.'
          end

          protected

          def any(regex)
            @string.scan(regex).flatten.map(&:strip).map(&:present?).any?
          end
        end
      end
    end
  end
end
