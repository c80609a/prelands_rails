# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      #
      # Проверит наличие файлов в zip-архиве и их требуемую вложенность.
      #
      class ValidateIncomingFiles
        include ::PrelandsRails::AbstractInteractor
        include ::Interactor
        include ::Interactor::Contracts
        include ::PrelandsRails::Base

        expects do
          required(:incoming_files).value(type?: Array)                       # список всех файлов архива { ftype, name }
        end

        assures do
        end

        def act
          zip_archive = ZipArchive.new context.incoming_files
          unless zip_archive.valid?
            fail! errors: zip_archive.errors.join('; ')
          end
        end
      end
    end
  end
end
