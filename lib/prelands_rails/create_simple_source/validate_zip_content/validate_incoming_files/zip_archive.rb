# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      class ValidateIncomingFiles
        class ZipArchive
          attr_reader :errors

          # @param [Array<{ftype, name}>] incoming_files
          def initialize(incoming_files)
            @errors         = []
            @incoming_files = incoming_files
          end

          def valid?
            @errors = [
              check_app_js,
              check_app_css,
              check_favicon,
              check_svg_absence
            ].compact

            @errors.none?
          end

          private

          # @return [nil]     Если `index.js` находится в корне zip-архива.
          # @return [String]  Иначе - вернёт сообщение об ошибке
          def check_app_js
            check 'index.js', 'index.js',
                  'The «index.js» JAVASCRIPT file is not found in the zip-archive',
                  'The «index.js» JAVASCRIPT file is not in root of the zip-archive: expected «index.js», got «%s»'
          end

          def check_app_css
            check 'index.css', 'index.css',
                  'The «index.css» STYLES file is not found in the zip-archive',
                  'The «index.css» STYLES file is not in root of the zip-archive: expected «index.css», got «%s»'
          end

          def check_favicon
            check 'icon.ico', /icon\.ico/,
                  'favicon file is not found in the zip-archive',
                  'favicon file is not in root of the zip-archive: got «%s»'
          end

          def check_svg_absence
            checka /\.svg/, 'svg files detected, move inside css'
          end

          protected

          def check(pattern, name, absent_error, wrong_place_error)
            error = absent_error

            @incoming_files.each do |file|
              if file.name[pattern]
                error = nil
                unless file.name.index(name)&.zero?
                  error = wrong_place_error % file.name
                end
                break
              end
            end

            error
          end

          # check absence
          def checka(pattern, present_error)
            error = nil

            @incoming_files.each do |file|
              if file.name[pattern]
                error = present_error
                break
              end
            end

            error
          end
        end
      end
    end
  end
end
