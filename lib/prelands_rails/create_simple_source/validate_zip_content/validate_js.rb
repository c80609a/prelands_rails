# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      #
      # Валидирует JavaScript код.
      #
      class ValidateJs
        include ::PrelandsRails::AbstractInteractor
        include ::Interactor
        include ::Interactor::Contracts

        expects do
          required(:files_content).value(type?: Hash) # { file_name => file_content, ... }
        end

        assures do
          optional(:warnings).maybe(type?: Array)
        end

        before do
          context.warnings ||= []
        end

        def act
          content = context[:files_content]['index.js']
          content = Js.new(content)
          context.warnings += content.errors unless content.valid?
        end

        on_breach { |breaches| bad_expects breaches }
      end
    end
  end
end
