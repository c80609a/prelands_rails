# frozen_string_literal: true

module PrelandsRails
  class CreateSimpleSource
    class ValidateZipContent
      class ValidateJs
        #
        # Принимает строку с JavaScript кодом разметкой и валидирует её.
        #
        class Js
          attr_reader :errors

          # @param [String] string Содержимое JavaScript файла.
          def initialize(string)
            @errors = []
            @string = string.gsub(/(\r\n?|\n)/, '') # убираем переносы, мешающие регуляркам
          end

          def valid?
            @errors = [
              check_location_changes,
              check_framework_calls
            ].compact

            !@errors.any?
          end

          private

          def check_location_changes
            match_data    = @string.match LOCATION_RX
            is_loc_assign = ->(matched) { matched =~ /^('|")$/ || @string.match(/(;|\s*)#{matched}\s*=\s*('|")/) }
            return unless match_data && is_loc_assign[match_data[3]]

            'Location statement has found'
          end

          def check_framework_calls
            res = FUNCS_RX.map do |key, func|
              [ key, !!@string[func] ]
            end

            return if res.map { |kf| kf[1] }.all?

            res.to_h.map do |key, result|
              'Call of %s() not found' % key if !result
            end.compact.join('; ')
          end

          private

          LOCATION_RX = /(;|\s*)location(\.href\s*=|\.replace\(|\.assign\(|\s*=)\s*(\w+|"|')/
          FUNCS_RX = {
            post_email:  /post_email\([^)]+\)/,
            post_gender: /post_gender\([^)]+\)/,
            post_age:    /post_age\([^)]+\)/
          }.freeze
        end
      end
    end
  end
end
