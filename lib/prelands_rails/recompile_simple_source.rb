# frozen_string_literal: true

module PrelandsRails
  #
  # Перекомпилировать преленд на основе уже загруженного в aws архива.
  #
  class RecompileSimpleSource
    include ::PrelandsRails::AbstractInteractor
    include ::Interactor::Organizer
    include ::Interactor::Contracts

    expects do
      required(:prelanding).filled#.value(type?: ::Preland)
      required(:static_js_path).value(type?: String)
      required(:s3_credentials).schema do
        required(:access_key).filled(:str?)
        required(:secret_key).filled(:str?)
        required(:region).filled(:str?)
      end
      required(:bucket_names).value(type?: Array)
      required(:model_preland_simple_source).filled
    end

    before do
      context.expected_locales = context.prelanding.locales.map(&:short_name)
      context.aws_prefix       = context.prelanding.preland_simple_source.aws_prefix
    end

    assures do
      required(:expected_locales).filled(type?: Array)
      required(:aws_prefix).value(type?: String)
      required(:archive).filled
      required(:tmp_dest).value(type?: String)
      required(:incoming_locales).filled(type?: Array)
      required(:files_content).value(type?: Hash)
      required(:compiled_htmls).value(type?: Hash)
      required(:old_aws_prefix).value(type?: String)
      required(:is_zip_moved).filled(:bool?)
    end

    after do
      clear_tmp_dest
      context.prelanding.preland_simple_source.update_columns updated_at: Time.now
    end

    organize DownloadZip,
             ::PrelandsRails::CreateSimpleSource::DetectIncomingLocales,
             ::PrelandsRails::CreateSimpleSource::CheckZipFiles,
             ::PrelandsRails::CreateSimpleSource::Compile,
             ::PrelandsRails::UpdateSimpleSource::GenerateNewAwsPrefix,
             Upload,
             UpdateRecord,                                                      # update aws_prefix
             ::PrelandsRails::UpdateSimpleSource::RemoveContentFromOldAwsPrefix,
             RenameZipFile

    on_breach { |breaches| bad_expects breaches }

    private

    def clear_tmp_dest
      td = context.tmp_dest
      File.delete(td) if File.exist?(td)
    end
  end
end
