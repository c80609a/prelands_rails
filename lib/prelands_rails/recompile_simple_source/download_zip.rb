# frozen_string_literal: true

module PrelandsRails
  class RecompileSimpleSource
    #
    # Скачает и разместит в cache-директории zip-архив с исходником лендинга.
    #
    class DownloadZip
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:prelanding).filled #.value(type?: ::Preland)
      end

      assures do
        required(:archive).filled                                               # Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:tmp_dest).value(type?: String)                                # путь к временному файлу
      end

      def act
        archive_url = prelanding.preland_simple_source.archive.url
        destination = make_tmp_path

        IO.copy_stream(Kernel.open(archive_url), destination)

        context.archive  = ActionDispatch::Http::UploadedFile.new(tempfile: destination)
        context.tmp_dest = destination
      end

      on_breach { |breaches| bad_expects breaches }

      delegate :prelanding,
               :to => :context
    end
  end
end
