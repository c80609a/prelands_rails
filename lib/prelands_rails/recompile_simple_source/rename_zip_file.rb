# frozen_string_literal: true

module PrelandsRails
  class RecompileSimpleSource
    class RenameZipFile
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts

      expects do
        required(:aws_prefix).value(type?: String)
        required(:old_aws_prefix).value(type?: String)
        required(:s3_credentials).schema do
          required(:access_key).filled(:str?)
          required(:secret_key).filled(:str?)
          required(:region).filled(:str?)
        end
        required(:zip_bucket_name).filled(:str?)
      end

      assures do
        required(:is_zip_moved).filled(:bool?)
      end

      def act
        old_name = '%s/src.zip' % context.old_aws_prefix
        new_name = '%s/src.zip' % context.aws_prefix

        context.is_zip_moved =
          client.move_file old_name, new_name, context.zip_bucket_name
      end

      private

      def client
        return @client if defined?(@client)

        creds   = context.s3_credentials
        @client = ::PrelandsRails::MyAwsClient.new creds[:access_key], creds[:secret_key], creds[:region]
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
