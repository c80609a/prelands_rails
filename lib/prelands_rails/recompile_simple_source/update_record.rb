# frozen_string_literal: true

module PrelandsRails
  class RecompileSimpleSource
    #
    # обновит запись ::Preland::SimpleSource
    #
    class UpdateRecord
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:aws_prefix).value(type?: String)
        required(:old_aws_prefix).value(type?: String)
        required(:model_preland_simple_source).filled
      end

      assures do
        required(:preland_simple_source).filled
      end

      def act
        source.aws_prefix = context.aws_prefix
        source.save!
        context.preland_simple_source = source
      end

      private

      def source
        @source ||= context.model_preland_simple_source.find_by aws_prefix: context.old_aws_prefix
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
