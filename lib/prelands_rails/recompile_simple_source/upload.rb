# frozen_string_literal: true

module PrelandsRails
  class RecompileSimpleSource
    #
    # Сначала удалить файлы преленда из директории +aws_prefix+, затем загрузить.
    #
    class Upload
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::UpdateSimpleSource::Upload::DeleteCompiledFiles

      expects do
        required(:archive).filled                                               # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:compiled_htmls).value(type?: Hash)                            # скомпилированные исходники    { file_name[String] => compiled_content[String], ... }
        required(:s3_credentials).schema do
          required(:access_key).filled(:str?)
          required(:secret_key).filled(:str?)
          required(:region).filled(:str?)
        end
        required(:bucket_names).value(type?: Array)
        required(:aws_prefix).value(type?: String)
        required(:files_content).value(type?: Hash)                             # { file_name => file_content, ... }
      end

      assures do
      end

      def act
        delete_compiled_files
        result = ::PrelandsRails::CreateSimpleSource::Upload.call context
        fail! errors: result.errors unless result.success?
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
