# frozen_string_literal: true

module PrelandsRails
  #
  # Примет zip-файл с исходниками преленда и обновит исходники этого преленда.
  # В результате будет обновлён `aws_prefix` -- имя директории в букете,
  # что позволит избежать кэширования статического контента. . Другими словами,
  # при обновлении файлов не надо ждать, пока Amazon обновит кэш или
  # очищать кэш Amazon-а.
  #
  class UpdateSimpleSource
    include ::PrelandsRails::AbstractInteractor
    include ::Interactor::Organizer
    include ::Interactor::Contracts

    expects do
      required(:archive)                                                        # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
      required(:preland_domain_ids).value(type?: Array)
      required(:expected_locales).value(type?: Array)                           # Список локалей, для которых must were all index_<locale>.html files in the zip archive
      required(:s3_credentials).schema do
        required(:access_key).filled(:str?)
        required(:secret_key).filled(:str?)
        required(:region).filled(:str?)
      end
      required(:bucket_names).value(type?: Array)
      required(:aws_prefix).value(type?: String)                                # имя директории в букете
      required(:preland_id).filled
      required(:static_js_path).value(type?: String)                            # путь к размещённым в интернете нашим js скриптам
      required(:model_preland_simple_source).filled
    end

    before do

    end

    assures do
      required(:incoming_locales).filled(type?: Array)
      optional(:files_content).maybe(type?: Hash)                               # распакованные файлы из архива { file_name[String] => file_content[String], ... }
      optional(:compiled_htmls).value(type?: Hash)                              # скомпилированные исходники    { file_name[String] => compiled_content[String], ... }
      required(:preland_simple_source).filled
      optional(:warnings).maybe(type?: Array)
    end

    organize ::PrelandsRails::CreateSimpleSource::DetectIncomingLocales,
             CheckZipFiles,
             ValidateZipContent,
             Compile,
             GenerateNewAwsPrefix,
             Upload,                                                            # to new aws_prefix
             UpdateRecord,                                                      # also update aws_prefix
             RemoveContentFromOldAwsPrefix

    on_breach { |breaches| bad_expects breaches }
  end
end
