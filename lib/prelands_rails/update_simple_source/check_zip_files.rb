# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class CheckZipFiles
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts

      expects do
        required(:archive)                                                      # Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:expected_locales).value(type?: Array)
        required(:incoming_locales).filled(type?: Array)
      end

      assures do
        optional(:files_content).maybe(type?: Hash)                             # js,html,css: { file_name => file_content, ... }
      end

      before do

      end

      def act
        if context.archive
          result = ::PrelandsRails::CreateSimpleSource::CheckZipFiles.call context
          if result.success?
            context.files_content = result.files_content
          else
            fail!(errors: result.errors)
          end
        end
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
