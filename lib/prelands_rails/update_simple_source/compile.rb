# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class Compile
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts

      expects do
        optional(:files_content).maybe(type?: Hash)                             # js,html,css: { file_name => file_content, ... }
        required(:static_js_path).value(type?: String)
      end

      assures do
        optional(:compiled_htmls).value(type?: Hash)                            # { file_name[String] => compiled_content[String], ... }
      end

      before do

      end

      def act
        if context.files_content
          result = ::PrelandsRails::CreateSimpleSource::Compile.call context
          if result.success?
            context.compiled_htmls = result.compiled_htmls
          else
            fail!(errors: result.errors)
          end
        end
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
