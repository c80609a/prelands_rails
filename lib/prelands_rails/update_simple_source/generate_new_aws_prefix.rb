# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class GenerateNewAwsPrefix
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts

      expects do
        required(:aws_prefix).value(type?: String)
      end

      assures do
        required(:old_aws_prefix).value(type?: String)
      end

      def act
        context.old_aws_prefix = context.aws_prefix
        context.aws_prefix     = SecureRandom.uuid[0..7]
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
