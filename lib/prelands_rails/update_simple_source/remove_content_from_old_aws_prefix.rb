# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class RemoveContentFromOldAwsPrefix
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::UpdateSimpleSource::Upload::DeleteCompiledFiles

      expects do
        required(:old_aws_prefix).value(type?: String)
        required(:s3_credentials).schema do
          required(:access_key).filled(:str?)
          required(:secret_key).filled(:str?)
          required(:region).filled(:str?)
        end
        required(:bucket_names).value(type?: Array)
      end

      def act
        delete_compiled_files(context.old_aws_prefix)
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
