module PrelandsRails
  class UpdateSimpleSource
    #
    # обновит запись ::Preland::SimpleSource
    #
    class UpdateRecord
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include ::PrelandsRails::Base

      expects do
        required(:archive)                                                      # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        required(:aws_prefix).value(type?: String)
        required(:old_aws_prefix).value(type?: String)
        required(:preland_id).filled
        required(:preland_domain_ids).value(type?: Array)
        required(:incoming_locales).filled(type?: Array)
        required(:model_preland_simple_source).filled
      end

      assures do
        required(:preland_simple_source).filled
      end

      before do

      end

      def act
        source                    = context.model_preland_simple_source.find_by aws_prefix: context.old_aws_prefix
        source.archive            = context.archive if context.archive
        source.preland_domain_ids = context.preland_domain_ids
        source.locales            = context.incoming_locales
        source.aws_prefix         = context.aws_prefix
        source.save!
        context.preland_simple_source = source
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
