# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class Upload
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts
      include DeleteCompiledFiles

      expects do
        required(:archive)                                                      # Входящий архив с исходниками от фрилансера. Rack::Test::UploadedFile | ActionDispatch::Http::UploadedFile
        optional(:compiled_htmls).maybe(type?: Hash)                            # скомпилированные исходники    { file_name[String] => compiled_content[String], ... }
        required(:s3_credentials).schema do
          required(:access_key).filled(:str?)
          required(:secret_key).filled(:str?)
          required(:region).filled(:str?)
        end
        required(:bucket_names).value(type?: Array)
        required(:aws_prefix).value(type?: String)
        optional(:files_content).maybe(type?: Hash)                             # js,html,css: { file_name => file_content, ... }
      end

      assures do

      end

      before do

      end

      def act
        if context.archive && context.compiled_htmls && context.files_content
          delete_compiled_files
          ::PrelandsRails::CreateSimpleSource::Upload.call context
        end
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
