# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource
    class Upload
      #
      # Удалить из +bucket_names+ файлы преленда из директории +aws_prefix+.
      #
      module DeleteCompiledFiles
        def delete_compiled_files(that_aws_prefix = nil)
          aws_prefix = that_aws_prefix || context.aws_prefix
          creds      = context.s3_credentials
          client     = ::PrelandsRails::MyAwsClient.new creds[:access_key], creds[:secret_key], creds[:region]

          context.bucket_names.each do |bucket_name|
            files = client.list_objects bucket_name
            next unless files

            files = files.map do |file_name|
              { key: file_name } if file_name.index(aws_prefix) == 0
            end.compact

            next if files.empty?

            client.delete_objects bucket_name, files
          end
        end
      end
    end
  end
end
