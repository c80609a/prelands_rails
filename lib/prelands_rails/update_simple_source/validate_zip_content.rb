# frozen_string_literal: true

module PrelandsRails
  class UpdateSimpleSource

    class ValidateZipContent
      include ::PrelandsRails::AbstractInteractor
      include ::Interactor
      include ::Interactor::Contracts

      expects do
        optional(:files_content).maybe(type?: Hash)                             # js,html,css: { file_name => file_content, ... }
      end

      assures do
        optional(:warnings).maybe(type?: Array)
      end

      def act
        if context.files_content
          result = ::PrelandsRails::CreateSimpleSource::ValidateZipContent.call context
          if result.success?
            context.warnings = result.warnings
          else
            fail!(errors: result.errors)
          end
        end
      end

      on_breach { |breaches| bad_expects breaches }
    end
  end
end
