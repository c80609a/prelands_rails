# frozen_string_literal: true

module PrelandsRails
  #
  # Примет zip-файл с исходниками преленда и валидирует его.
  #
  class ValidateSimpleSource
    include ::PrelandsRails::AbstractInteractor
    include ::Interactor::Organizer
    include ::Interactor::Contracts

    expects do
      required(:archive).filled
      required(:expected_locales).filled(type?: Array)                          # Список локалей, для которых must were all index_<locale>.html files in the zip archive
    end

    assures do
      optional(:warnings).maybe(type?: Array)
    end

    organize ::PrelandsRails::CreateSimpleSource::DetectIncomingLocales,
             ::PrelandsRails::CreateSimpleSource::CheckZipFiles,
             ::PrelandsRails::CreateSimpleSource::ValidateZipContent

    on_breach { |breaches| bad_expects breaches }
  end
end
