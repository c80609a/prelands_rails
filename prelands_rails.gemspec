if File.exist?('lib/prelands_rails/version')
  lib = File.expand_path("lib", __dir__)
  $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
end

Gem::Specification.new do |spec|
  spec.name          = "prelands_rails"
  spec.version       = "0.1.5"
  spec.authors       = ["C80609A"]
  spec.email         = ["c080609a@gmail.com"]

  spec.summary       = %q{Код, занимающийся валидацией и публикацией прелендов V4.}
  spec.homepage      = "https://gitlab.com/c80609a/prelands_rails"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")


  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/c80609a/prelands_rails"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.rdoc_options  = ['--charset=UTF-8']

  spec.add_dependency 'interactor', '~> 3.0'
  spec.add_dependency 'interactor-contracts'
  spec.add_dependency 'rubyzip', '> 1.0.0'
  spec.add_dependency 'aws-sdk'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'mimemagic'
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rails', '6.0.2.1'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'awesome_print'
  spec.add_development_dependency 'factory_bot_rails'
  spec.add_development_dependency 'database_cleaner'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-rails', '~> 4.0'
  spec.add_development_dependency 'rails-controller-testing'
  spec.add_development_dependency 'paperclip', '5.0.0'
  spec.add_development_dependency 'tzinfo-data'
end
