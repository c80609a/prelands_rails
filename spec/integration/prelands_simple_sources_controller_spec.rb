require 'rails_helper'

RSpec.describe ::Prelands::SimpleSourcesController, type: :controller do
  # mock aws
  before do
    double = double '::PrelandsRails::MyAwsClient'
    allow(double).to receive(:upload_file)
    allow(double).to receive(:list_objects)
    allow(double).to receive(:move_file).and_return(true)
    allow(::PrelandsRails::MyAwsClient).to receive(:new).and_return(double)
  end

  # mock env
  before do
    ENV['AWS_ACCESS_KEY_ID']      = 'an_access_key'
    ENV['AWS_SECRET_ACCESS_KEY']  = 'a_secret_key'
    ENV['AWS_S3_REGION']          = 'a_region'
    ENV['PRELAND_STATIC_JS_PATH'] = 'https://js.path'

    PRELAND_COMPILED_CONFIG = {
      access_key: ENV['AWS_ACCESS_KEY_ID'],
      secret_key: ENV['AWS_SECRET_ACCESS_KEY'],
      region:     ENV['AWS_S3_REGION']
    }
  end

  describe 'Когда требуются записи и база данных' do
    let!(:domain) { create(:preland_domain, url: 'https://date4more.eu') }
    let!(:preland) { create(:preland, :new_api) }

    describe 'POST #create' do
      before { post :create, params: params }
      let(:params) do
        {
          preland_simple_source: {
            archive:            z('nominal.zip'),
            preland_domain_ids: [domain.id]
          },
          preland_id: preland.id
        }
      end

      it { expect(response).to have_http_status(:created) }
      it { expect(JSON.parse(response.body)).to eq({'notice' => 'Success!'}) }
    end

    describe 'PUT #update' do
      before { put :update, params: params }

      let!(:simple_source) { create(:preland_simple_source, preland: preland, preland_domain_ids: [domain.id]) }

      let!(:params) do
        {
          preland_id: preland.id,
          id:         simple_source.id,
          preland_simple_source: {
            archive:            z('nominal.zip'),
            preland_domain_ids: [domain.id]
          }
        }
      end

      it { expect(response).to have_http_status(:ok) }
      it { expect(JSON.parse(response.body)).to eq({'notice' => 'Success!'}) }
    end

    describe 'POST #recompile' do
      # mock DownloadZip
      before do
        expect(::PrelandsRails::RecompileSimpleSource::DownloadZip).to receive(:call!) do |context|
          context.archive = z('nominal.zip')
          context.tmp_dest = '/tmp/rspec_dest'
        end
      end

      let(:params) {{ preland_id: preland.id }}

      let!(:simple_source) { create(:preland_simple_source, preland: preland, preland_domain_ids: [domain.id]) }

      before { post :recompile, params: params }

      it { expect(response).to have_http_status(:ok) }
      it { expect(JSON.parse(response.body)).to eq({'notice' => 'Success!'}) }
    end
  end

  describe 'Когда записи и база данных не требуются' do
    describe 'POST #validate' do
      before { post :validate, params: params }

      context 'success' do
        let(:params) do
          {
            preland_simple_source: {
              archive:          z('nominal.zip'),
              expected_locales: ENV['EXPECTED_LOCALES']&.split(',') || ['ru', 'en']
            }
          }
        end

        it { expect(response).to have_http_status(:ok) }
        it('returns valid notice', focus: 'int_spec') { expect(JSON.parse(response.body)).to eq({'notice' => 'valid'}) }
      end

      context 'fail' do
        context 'Если локали, которые были указаны, не совпадают с локалями, которые были найдены в zip-архиве' do
          let(:params) do
            {
              preland_simple_source: {
                archive:          z('nominal.zip'),
                expected_locales: ['kz']
              }
            }
          end

          it { expect(response).to have_http_status(422) }
          it { expect(JSON.parse(response.body)).to eq({'errors' => 'index_kz.html not found'}) }
        end
      end
    end
  end
end