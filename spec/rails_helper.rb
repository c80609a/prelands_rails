# frozen_string_literal: true

require 'spec_helper'
require 'rails/all'
require 'rspec/rails'
require 'support/rails_app/config/environment'
require 'rails-controller-testing'

Rails::Controller::Testing.install
ActiveRecord::Migration.maintain_test_schema!

# set up db
# be sure to update the schema if required by doing
# - cd spec/rails_app
# - rake db:migrate
ActiveRecord::Schema.verbose = false
load 'support/rails_app/db/schema.rb' # use db agnostic schema by default

RSpec.configure do |config|
  config.fixture_path = File.expand_path('../fixtures', __FILE__)
  config.include PrelandsRailsShared
  config.before(:suite) { DatabaseCleaner.clean_with :truncation }
  config.before(:each) { DatabaseCleaner.strategy = :transaction }
  config.before(:each) { DatabaseCleaner.start }
  config.append_after(:each) { DatabaseCleaner.clean }
end