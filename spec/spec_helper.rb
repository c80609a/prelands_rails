ENV["RAILS_ENV"] = "test"

require 'bundler/setup'
require 'rubygems'
require 'byebug'
require 'paperclip'
require 'awesome_print'
require 'factory_bot'
require 'factory_bot_rails'
require 'database_cleaner'

Dir[File.expand_path('../support/**/*.rb', __FILE__)].each do |file|
  require file unless file.include?('support/rails_app')
end

require 'prelands_rails'

RSpec.configure do |config|
  config.filter_run :focus => ENV['RSPEC_FOCUS'] if !!ENV['RSPEC_FOCUS']
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
