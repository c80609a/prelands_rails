# frozen_string_literal: true

module PreloadHelper
  extend self

  def load(name)
    File.read(File.join(preload_path, name))
  end

  def preload_path
    fixtures_path = File.expand_path('../../fixtures/files/html', ::Rails.root)
    File.expand_path(fixtures_path, __FILE__)
  end
end

# shortcuts
module PrelandsRailsShared
  def l(file)
    PreloadHelper.load file
  end

  def z(name)
    fixture_file_upload('files/%s' % name, 'application/zip')
  end
end
