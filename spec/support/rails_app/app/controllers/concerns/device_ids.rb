module DeviceIds
  COMMON_DEVICE = 'both'.freeze
  DEVICES = { COMMON_DEVICE.to_sym => 1, mobile: 2, web: 3 }.freeze

  extend ActiveSupport::Concern

  included do
    enum device_id: DEVICES
  end
end
