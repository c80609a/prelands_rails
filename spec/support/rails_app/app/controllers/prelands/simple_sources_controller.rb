# frozen_string_literal: true

module Prelands
  class SimpleSourcesController < ApplicationController
    before_action :find_preland, only: %i[create update recompile]
    before_action :find_source,  only: %i[update]

    def create
      result = ::PrelandsRails::CreateSimpleSource.call create_params

      if result.success?
        render json: { notice: 'Success!' }, status: :created
      else
        message = result.errors.is_a?(Hash) ? result.errors.inspect : result.errors
        render json: { errors: message }, status: :unprocessable_entity
      end
    end

    def update
      result = ::PrelandsRails::UpdateSimpleSource.call update_params

      if result.success?
        render json: { notice: 'Success!' }, status: :ok
      else
        message = result.errors.is_a?(Hash) ? result.errors.inspect : result.errors
        render json: { errors: message }, status: :unprocessable_entity
      end
    end

    def validate
      result = ::PrelandsRails::ValidateSimpleSource.call validate_params

      if result.success?
        render json: { notice: 'valid' }, status: :ok
      else
        message = result.errors.is_a?(Hash) ? result.errors.inspect : result.errors
        render json: { errors: message }, status: :unprocessable_entity
      end
    end

    def recompile
      result = ::PrelandsRails::RecompileSimpleSource.call recompile_params
      if result.success?
        render json: { notice: 'Success!' }, status: :ok
      else
        message = result.errors.is_a?(Hash) ? result.errors.inspect : result.errors
        render json: { errors: message }, status: :unprocessable_entity
      end
    end

    private

    def create_params
      result =
        params
          .fetch(:preland_simple_source, {})
          .permit(:archive, preland_domain_ids: [])

      preland_domain_ids = result.to_h['preland_domain_ids']

      bucket_names =
        if preland_domain_ids
          Preland::Domain.where(id: preland_domain_ids).pluck(:aws_bucket)
        end

      result.to_h.merge({
        preland_domain_ids:          (preland_domain_ids || []).select { |id| id != '' },
        expected_locales:            @preland.locales.map(&:short_name),                   # ожидаем, что фрилансер закодил все локали
        aws_prefix:                  SecureRandom.uuid[0..7],                              # тут же генерим имя директории внутри bucket-а на AWS, в которую будем грузить скомпилированные исходники
        bucket_names:                bucket_names,
        s3_credentials:              ::PRELAND_COMPILED_CONFIG,
        preland_id:                  @preland.id,
        static_js_path:              ENV['PRELAND_STATIC_JS_PATH'],
        model_preland_simple_source: ::Preland::SimpleSource
      })
    end

    def update_params
      create_params.tap do |params|
        params[:aws_prefix]         = @source.aws_prefix
        params[:preland_domain_ids] = @source.preland_domain_ids                  # NOTE:: пока запрещаем менять домены при уже загруженных исходниках
      end
    end

    def validate_params
      result =
        params
          .fetch(:preland_simple_source, {})
          .permit(:archive, expected_locales: [])

      result.to_h.merge({ })
    end

    def recompile_params
      {
        prelanding:                  @preland,
        static_js_path:              ENV['PRELAND_STATIC_JS_PATH'],
        s3_credentials:              ::PRELAND_COMPILED_CONFIG,
        bucket_names:                Preland::Domain.where(id: @preland.preland_simple_source.preland_domain_ids).pluck(:aws_bucket),
        model_preland_simple_source: ::Preland::SimpleSource,
        zip_bucket_name:             'pspzips'
      }
    end

    def find_preland
      @preland = Preland.find params[:preland_id]
    end

    def find_source
      @source = ::Preland::SimpleSource.find(params[:id])
    end
  end
end