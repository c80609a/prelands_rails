# frozen_string_literal: true

class Preland < ActiveRecord::Base
  include DeviceIds

  scope :new_api, -> { where(new_api: true) }
  scope :old_api, -> { where(new_api: false) }

  has_one :preland_simple_source, class_name: 'Preland::SimpleSource', dependent: :destroy

  scope :v4,      proc { where(p_ver: V4) }
  scope :new_api, proc { where(new_api: true )}
  scope :mobile,  proc { where.not(id: 0).where(device_id: ::Dicts::Device::MOBILE_BOTH.map(&:id)) }
  scope :web,     proc { where.not(id: 0).where(device_id: ::Dicts::Device::WEB_BOTH.map(&:id)) }

  V4 = 4
  def v4?
    p_ver == V4
  end

  def locales
    [
      {short_name: 'en'},
      {short_name: 'ru'}
    ].map(&:to_struct)
  end
end
