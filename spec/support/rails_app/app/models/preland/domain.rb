# == Schema Information
#
# Table name: preland_domains
#
#  id         :integer          not null, primary key
#  aws_bucket :string(40)       not null
#  url        :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Preland
  class Domain < ActiveRecord::Base
    self.table_name = 'preland_domains'
    DOMAIN_REGEXP = /\Ahttps?:\/\/(www\.)?[-a-zA-Z0-9а-я@:%._\+~#=]{1,256}\.[a-zA-Z0-9а-я()]{1,6}\b([-a-zA-Z0-9а-я()@:%_\+.~#?&\/\/={}\[\]]*)\z/mi

    has_many(
      :preland_domain_sources, class_name: 'Preland::DomainSource', dependent: :destroy, foreign_key: :preland_domain_id
    )

    validates :url, presence: true, format: DOMAIN_REGEXP, uniqueness: true
  end
end
