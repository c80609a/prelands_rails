# == Schema Information
#
# Table name: preland_domain_sources
#
#  id                :integer          not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  preland_domain_id :integer
#  preland_source_id :integer
#
# Indexes
#
#  index_preland_domain_sources_on_preland_domain_id  (preland_domain_id)
#  index_preland_domain_sources_on_preland_source_id  (preland_source_id)
#

class Preland
  class DomainSource < ActiveRecord::Base
    self.table_name = 'preland_domain_sources'
    belongs_to :preland_source, class_name: 'Preland::SimpleSource', optional: true
    belongs_to :preland_domain, class_name: 'Preland::Domain', optional: true
  end
end
