# frozen_string_literal: true

# == Schema Information
#
# Table name: preland_sources
#
#  id                   :integer          not null, primary key
#  archive_content_type :string(255)
#  archive_file_name    :string(255)
#  archive_file_size    :integer
#  archive_updated_at   :datetime
#  aws_prefix           :string(255)      not null
#  locales              :string(255)      not null
#  status               :integer          default(0), not null
#  type                 :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  preland_id           :integer
#
# Indexes
#
#  index_preland_sources_on_preland_id  (preland_id)
#

class Preland
  class SimpleSource < ActiveRecord::Base
    self.table_name = 'preland_sources'

    belongs_to :preland

    has_many(
      :preland_domain_sources, class_name: 'Preland::DomainSource', dependent: :destroy, foreign_key: :preland_source_id
    )
    has_many :preland_domains, through: :preland_domain_sources

    serialize :locales, Array

    include ::Uploaders::Preland::SimpleSource
    validates :archive, attachment_presence: true
    validates_attachment_content_type :archive, content_type: %w[application/zip application/x-zip application/x-zip-compressed].freeze
  end
end
