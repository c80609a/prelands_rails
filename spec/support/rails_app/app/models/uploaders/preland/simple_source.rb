module Uploaders
  module Preland
    module SimpleSource
      extend ActiveSupport::Concern

      included do
        has_attached_file :archive, **::PAPERCLIP_PRELAND_ZIP_SOURCES_CONFIG

        def key
          File.join(aws_prefix, 'src.zip')
        end

        Paperclip.interpolates :key do |attachment, _|
          attachment.instance.key
        end
      end
    end
  end
end
