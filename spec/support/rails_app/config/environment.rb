# Load the Rails application.
require_relative 'application'

# gem should be required here
require 'prelands_rails'

# Initialize the Rails application.
Rails.application.initialize!
