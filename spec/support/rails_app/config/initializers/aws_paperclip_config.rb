# frozen_string_literal: true

PAPERCLIP_PRELAND_ZIP_SOURCES_CONFIG = { storage: :filesystem }

PRELAND_COMPILED_CONFIG = {
  access_key: ENV['AWS_ACCESS_KEY_ID'],
  secret_key: ENV['AWS_SECRET_ACCESS_KEY'],
  region:     ENV['AWS_S3_REGION']
}
