Rails.application.routes.draw do
  namespace :prelands do
    resources :simple_sources, path: '/:preland_id/simple_sources', only: %i[create update]
    post :validate,  to: 'simple_sources#validate'
    post :recompile, to: 'simple_sources#recompile'
  end
end
