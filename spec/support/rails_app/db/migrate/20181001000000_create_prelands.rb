class CreatePrelands < ActiveRecord::Migration[4.2]
  def change
    create_table 'prelands', force: :cascade do |t|
      t.string   'name',        limit: 255
      t.integer  'device_id',   limit: 4,   default: 1
      t.datetime 'created_at',                              null: false
      t.datetime 'updated_at',                              null: false
      t.integer  'category_id', limit: 3
      t.boolean  'new_api',                 default: false, null: false
      t.integer  'p_ver',       limit: 4,   default: 0
    end
  end
end