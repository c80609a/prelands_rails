class CreatePrelandSources < ActiveRecord::Migration[4.2]
  def change
    create_table :preland_sources do |t|
      t.string :type
      t.string :aws_prefix
      t.integer :status, default: 0, null: false, limit: 1
      t.attachment :archive
      t.text :compilation_info
      t.references :preland, index: true

      t.timestamps null: false
    end
  end
end
