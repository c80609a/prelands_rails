class CreatePrelandDomains < ActiveRecord::Migration[4.2]
  def change
    create_table 'preland_domains', force: :cascade do |t|
      t.string   'url',        limit: 255
      t.datetime 'created_at',             null: false
      t.datetime 'updated_at',             null: false
      t.string   'aws_bucket', limit: 40,  null: false
    end
  end
end
