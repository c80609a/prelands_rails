class CreatePrelandDomainSources < ActiveRecord::Migration[4.2]
  def change
    create_table 'preland_domain_sources', force: :cascade do |t|
      t.integer  'preland_source_id', limit: 4
      t.integer  'preland_domain_id', limit: 4
      t.datetime 'created_at',                  null: false
      t.datetime 'updated_at',                  null: false
    end

    add_index 'preland_domain_sources', ['preland_domain_id'], name: 'index_preland_domain_sources_on_preland_domain_id', using: :btree
    add_index 'preland_domain_sources', ['preland_source_id'], name: 'index_preland_domain_sources_on_preland_source_id', using: :btree

  end
end
