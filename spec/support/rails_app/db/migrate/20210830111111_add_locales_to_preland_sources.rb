# Денормализация данных - при обработке архива с исходниками запомним локали,
# для которых этот исходник сделан, чтобы когда менеджер меняет локали преленда,
# можно было без дополнительной скачки с AWS и распаковки архива выяснить наличие
# локалей исходника.
class AddLocalesToPrelandSources < ActiveRecord::Migration[4.2]
  def change
    add_column :preland_sources, :locales, :string
  end
end
