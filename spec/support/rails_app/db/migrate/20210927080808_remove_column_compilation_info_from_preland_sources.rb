class RemoveColumnCompilationInfoFromPrelandSources < ActiveRecord::Migration[4.2]
  def change
    remove_column :preland_sources, :compilation_info
  end
end
