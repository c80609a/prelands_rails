class ChangeColumnNullAwsPrefixToPrelandSources < ActiveRecord::Migration[4.2]
  def change
    change_column :preland_sources, :aws_prefix, :string, null: false
  end
end
