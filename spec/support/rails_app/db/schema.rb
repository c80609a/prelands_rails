# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_29_090909) do

  create_table "preland_domain_sources", force: :cascade do |t|
    t.integer "preland_source_id", limit: 4
    t.integer "preland_domain_id", limit: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["preland_domain_id"], name: "index_preland_domain_sources_on_preland_domain_id"
    t.index ["preland_source_id"], name: "index_preland_domain_sources_on_preland_source_id"
  end

  create_table "preland_domains", force: :cascade do |t|
    t.string "url", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "aws_bucket", limit: 40, null: false
  end

  create_table "preland_sources", force: :cascade do |t|
    t.string "type"
    t.string "aws_prefix", null: false
    t.integer "status", limit: 1, default: 0, null: false
    t.string "archive_file_name"
    t.string "archive_content_type"
    t.integer "archive_file_size"
    t.datetime "archive_updated_at"
    t.integer "preland_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "locales"
    t.index ["preland_id"], name: "index_preland_sources_on_preland_id"
  end

  create_table "prelands", force: :cascade do |t|
    t.string "name", limit: 255
    t.integer "device_id", limit: 4, default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category_id", limit: 3
    t.boolean "new_api", default: false, null: false
    t.integer "p_ver", limit: 4, default: 0
  end

end
