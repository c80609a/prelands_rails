# frozen_string_literal: true

include ActionDispatch::TestProcess

FactoryBot.define do
  factory :preland, class: Preland do
    name { 'preland' % rand(10..99) }
    trait :new_api do
      new_api {true}
    end
  end

  factory :preland_domain, class: Preland::Domain do
    url { Faker::Internet.domain_name }
    aws_bucket { SecureRandom.uuid[0..7] }
  end

  factory :preland_simple_source, class: Preland::SimpleSource do
    preland { create(:preland, :new_api) }
    archive { fixture_file_upload(File.expand_path('../../fixtures/files/nominal.zip', ::Rails.root), 'application/zip') }
    aws_prefix { SecureRandom.uuid[0..7] }
    locales { ['en'] }
  end
end