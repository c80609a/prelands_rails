# frozen_string_literal: true

require 'rails_helper'

module DetectAbsentFilesSpec
  class Subj
    include ::PrelandsRails::CreateSimpleSource::CheckZipFiles::DetectAbsentFiles
  end
end

RSpec.describe DetectAbsentFilesSpec::Subj do
  subject { described_class.new.detect_absent_files expected_files, incoming_files }

  let(:expected_files) do
    [
      { ftype: :directory, name: 'images/'         },
      { ftype: :file,      name: 'index.css'       },
      { ftype: :file,      name: 'index.js'        },
      { ftype: :file,      name: 'index_ru.html'   }
    ].map(&:to_struct)
  end

  context 'Nominal - все файлы на месте' do
    let(:incoming_files) do
      [
        { ftype: :directory, name: 'images/'         },
        { ftype: :file,      name: 'images/girl.gif' },
        { ftype: :file,      name: 'index.css'       },
        { ftype: :file,      name: 'index.js'        },
        { ftype: :file,      name: 'index_ru.html'   }
      ].map(&:to_struct)
    end

    it 'Вернёт пустой массив' do
      expect(subject).to eq([])
    end
  end

  context 'Каких-то файлов нет' do
    let(:incoming_files) do
      [
        # { ftype: :directory, name: 'images/'         },
        { ftype: :file,      name: 'images/girl.gif' },
        { ftype: :file,      name: 'index.css'       },
        # { ftype: :file,      name: 'index.js'        },
        { ftype: :file,      name: 'index_ru.html'   }
      ].map(&:to_struct)
    end

    it 'Вернёт список строк с отсутствующими файлами' do
      expect(subject).to eq(['images/ not found', 'index.js not found'])
    end
  end

  context '' do
    context 'Если подан incoming_files=nil' do
      let(:incoming_files) { nil }
      it :works do
        expect(subject).to eq(['images/ not found', 'index.css not found', 'index.js not found', 'index_ru.html not found'])
      end
    end

    context 'Если подан expected_files=(nil|[])' do
      let(:expected_files) { [nil,[]].sample }
      let(:incoming_files) do
        [
          { ftype: :directory, name: 'images/'         },
          { ftype: :file,      name: 'images/girl.gif' },
          { ftype: :file,      name: 'index.css'       },
          { ftype: :file,      name: 'index.js'        },
          { ftype: :file,      name: 'index_ru.html'   }
        ].map(&:to_struct)
      end

      it 'raises exception' do
        expect{ subject }.to raise_exception 'expected_files must be present'
      end
    end
  end
end
