# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::CheckZipFiles do
  subject { described_class.call params }

  shared_examples :works do
    it 'Читает в память все имеющиеся в архиве index_<locale>.html' do
      expect([
        subject.success?,
        subject.errors,
        subject.files_content['index_ru.html'].present?,
        subject.files_content['index_en.html'].present?
      ]).to eq [
        true,
        nil,
        true,
        true
      ]
    end
  end

  context 'Nominal' do
    let(:params) do
      {
        archive:            z('nominal.zip'),
        preland_domain_ids: [1],
        expected_locales:   ['ru'],
        incoming_locales:   ['ru', 'en'],
      }
    end

    include_examples :works
  end

  context 'Нет ожидаемой локали' do
    let(:params) do
      {
        archive:            z('nominal.zip'),
        preland_domain_ids: [1],
        expected_locales:   ['ru', 'es'],
        incoming_locales:   ['ru', 'en'],
      }
    end

    it(:works) { expect([subject.success?, subject.errors]).to eq [false, 'index_es.html not found'] }
  end

  context 'File too large' do
    let(:params) do
      {
        archive:            z('too_large.zip'),
        preland_domain_ids: [1],
        expected_locales:   ['ru', 'en'],
        incoming_locales:   ['ru', 'en'],
      }
    end
    it(:works) { expect([subject.success?, subject.errors]).to eq [false, 'File too large when extracted (must be less than 8388608 bytes)'] }
  end
end
