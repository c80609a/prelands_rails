# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::Compile::HtmlCompiler do
  subject { described_class.new(content, index_css, index_js, lang, static_js_path) }

  let(:content)        { l 'index_en.html' }
  let(:index_css)      { rb l('index.css') }
  let(:index_js)       { rb l('index.js')  }
  let(:lang)           { 'en' }
  let(:static_js_path) { 'https://qwerty.s3.eu-central-1.amazonaws.com/static.js' }

  let(:expected_head) do
    res =
      <<-HTML
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Document</title>

<script async="" src="https://www.google-analytics.com/analytics.js"> </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
<link rel="shortcut icon" href="icon.ico">

<style>body { font-size: 18px; }</style>
</head>
      HTML
    res.gsub(/(\r\n?|\n)/, '')
  end

  let(:expected_body) do
    res =
      <<-HTML
<body>
  <h5>Ehlo, buddy</h5>
  <div id="continue">OKAY</div>
  <script src="index.js"> </script>
</body>#{static_js}
    HTML
    rb res
  end

  let(:expected_html) do
    tpl = '<!DOCTYPE html><html lang="%{lang}">%{head}%{body}</html>'
    tpl % {
      lang: lang,
      head: expected_head,
      body: expected_body
    }
  end

  context '@head' do
    it('makes head') do
      expect(subject.instance_variable_get(:@head)).to eq expected_head
    end
  end

  context '@body' do
    it('makes body') { expect(rb subject.instance_variable_get(:@body)).to eq expected_body }
  end

  context '@compiled' do
    it('Makes html') { expect(rb subject.compiled).to eq expected_html }
  end

  # remove breaks
  def rb(str)
    str.gsub(/(\r\n?|\n)/, '')
  end

  def static_js
    <<-JS
<script>
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.async = true;
    js.src = "#{static_js_path}?" + Math.random();
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'sdk'));
</script>
    JS
  end
end
