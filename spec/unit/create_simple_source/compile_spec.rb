# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::Compile do
  subject { described_class.call params }

  shared_examples :works do
    it(:works) { expect([subject.success?, subject.errors]).to eq [true, nil] }
  end

  before do
    expect(::PrelandsRails::CreateSimpleSource::Compile::HtmlCompiler).to receive(:new).and_return(OpenStruct.new(compiled: '1')).twice
  end

  let(:params) do
    {
      files_content: {
        'index.css'     => '1',
        'index_en.html' => '2',
        'index.js'      => '3',
        'index_ru.html' => '4'
      },
      static_js_path: 'https://qwerty.s3.eu-central-1.amazonaws.com/static.js'
    }
  end

  include_examples :works

  it do
    expect(subject.compiled_htmls['index_en.html']).to be_truthy
    expect(subject.compiled_htmls['index_ru.html']).to be_truthy
  end

end
