require 'rails_helper'

# doubles
module CreateRecordSpec
  class PrelandSimpleSource
    attr_reader :aws_prefix, :archive, :preland_id, :preland_domain_ids, :locales
    def initialize(params)
      @aws_prefix         = params[:aws_prefix]
      @archive            = params[:archive]
      @preland_id         = params[:preland_id]
      @preland_domain_ids = params[:preland_domain_ids]
      @locales            = params[:locales]
    end
    def self.create!(params)
      new params
    end
  end

  class Preland
    def id
      2
    end
  end

  class Domain
    def id
      1
    end
  end
end

RSpec.describe ::PrelandsRails::CreateSimpleSource::CreateRecord, focus: 'create_record_spec' do
  subject { described_class.call params }

  shared_examples :success do
    it(:works) { expect([subject.success?, subject.errors,]).to eq([true, nil]) }

    it { expect(subject.preland_simple_source.aws_prefix).to         eq params[:aws_prefix] }
    it { expect(subject.preland_simple_source.archive).to            eq params[:archive] }
    it { expect(subject.preland_simple_source.preland_id).to         eq params[:preland_id] }
    it { expect(subject.preland_simple_source.preland_domain_ids).to eq params[:preland_domain_ids] }
    it { expect(subject.preland_simple_source.locales).to            eq params[:incoming_locales] }
  end

  let(:params) do
    {
      archive:                     z('nominal.zip'),
      aws_prefix:                  'abcd',
      preland_id:                  preland.id,
      preland_domain_ids:          [domain.id],
      incoming_locales:            ['en','ru'],
      model_preland_simple_source: ::CreateRecordSpec::PrelandSimpleSource
    }
  end

  let!(:preland) { ::CreateRecordSpec::Preland.new }
  let!(:domain)  { ::CreateRecordSpec::Domain.new }

  context 'Create' do
    include_examples :success
  end
end
