# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::DetectIncomingLocales do
  subject { described_class.call params }

  shared_examples :works do
    it(:works) { expect([subject.success?, subject.errors, subject.incoming_locales.sort]).to eq [true, nil, ['en','ru'].sort] }
  end

  context 'Nominal' do
    let(:params) do
      {
        archive: z('nominal.zip')
      }
    end

    include_examples :works
  end
end
