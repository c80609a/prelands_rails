# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::Upload do
  subject { described_class.call params }

  before do
    buckets  = params[:bucket_names].size
    images   = expected_images_names.size
    compiles = params[:compiled_htmls].keys.count
    files    = params[:files_content].keys.count

    double = double '::PrelandsRails::CreateSimpleSource::Upload::Uploader'
    expect(double).to receive(:upload_file).exactly(buckets * images)
    expect(double).to receive(:upload_content).exactly(buckets * (compiles + files))
    expect(::PrelandsRails::CreateSimpleSource::Upload::Uploader)
      .to receive(:new).and_return(double).once

    expect(File).to receive(:delete).exactly(images).and_call_original
  end

  shared_examples :works do
    it(:works) { expect([subject.success?, subject.errors]).to eq [true, nil] }
  end

  let(:params) do
    {
      compiled_htmls: {
        'index_ru.html' => '1',
        'index_en.html' => '2'
      },
      s3_credentials: { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
      bucket_names:   ['pstestbucket', 'date4more.eu'],
      aws_prefix:     '12312312',
      archive:        z('nominal.zip'),
      files_content:  { 'index.js' => 'alert("Hello, world")' }
    }
  end

  let(:expected_images_names) { ["icon.ico", "images/bg-1.jpg", "images/bg-1-desktop.jpg"] }

  include_examples :works

  it do
    uploaded_images = subject.uploaded_images.map { |k,_| k }
    expect(uploaded_images).to eq(expected_images_names)
  end
end
