require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::ValidateZipContent::ValidateCss do
  subject { described_class.call params }

  context 'Nominal' do
    shared_examples :success do
      it(:works) { expect([subject.success?, subject.errors]).to eq [true, nil] }
    end

    shared_examples :wrong do
      it 'Fails and return relevant error' do
        expect([subject.success?, subject.errors]).to eq [false, { 'index.css' => expected_errors }]
      end
    end

    let(:good_params) do
      {
        files_content: {
          'index.css' => l('index.css'),
        }
      }
    end

    context 'Nominal - valid css' do
      let(:params) { good_params }
      include_examples :success
    end

    context 'Invalid css' do
      context 'Link to svg file found' do
        let(:params)          { patch_one 'index.css', 'app_bad__svg_links_detected.css' }
        let(:expected_errors) {['Links to svg detected inside css file. Please, use inline variant of svg.']}
        include_examples :wrong
      end
      context 'Link to images must be relative' do
        let(:params)          { patch_one 'index.css', 'app_bad__images_wrong_path.css' }
        let(:expected_errors) {['Images urls in css files must be relative.']}
        include_examples :wrong
      end
      context 'Custom fonts found' do
        let(:params)          { patch_one 'index.css', 'app_bad__custom_fonts_found.css' }
        let(:expected_errors) {['Custom fonts detected in css files. Please, use Google fonts.']}
        include_examples :wrong
      end
    end
  end

  def patch_one(key, file)
    good_params.dup.tap do |p|
      p[:files_content][key] = l(file)
    end
  end
end
