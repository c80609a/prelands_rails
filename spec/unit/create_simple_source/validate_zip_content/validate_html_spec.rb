# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::ValidateZipContent::ValidateHtml do
  subject { described_class.call params }

  shared_examples :success do
    it(:works) { expect([subject.success?, subject.errors]).to eq [true, nil] }
  end

  shared_examples :wrong do
    it 'Fails and return relevant error' do
      expect([subject.success?, subject.errors]).to eq [false, { 'index_en.html' => expected_errors }]
    end
  end

  let(:good_params) do
    {
      files_content: {
        'index.css'     => l('index.css'),
        'index_en.html' => l('index_en.html'),
        'index.js'      => l('index.js'),
        #'index_ru.html' => l('index_ru.html'),
      }
    }
  end

  context 'Nominal - valid html' do
    let(:params) { good_params }
    include_examples :success
  end

  context 'Invalid html' do
    context 'Internal JS' do
      let(:params)          { patch_one 'index_en.html', 'index_en_bad_js_detected.html' }
      let(:expected_errors) {['A JavaScript code is detected inside html-file. Please, move it to index.js.']}
      include_examples :wrong
    end

    context 'Internal CSS' do
      let(:params)          { patch_one 'index_en.html', 'index_en_bad_css_detected.html' }
      let(:expected_errors) {['An internal CSS is detected inside html-file. Please, move it to index.css.']}
      include_examples :wrong
    end

    context 'Continue element' do
      context 'Not found' do
        let(:params)          { patch_one 'index_en.html', 'index_en_bad_continue_element_not_found.html' }
        let(:expected_errors) { ['Not found continue element with id="%s"' % described_class::CONTINUE_ID] }
        include_examples :wrong
      end

      context 'Not uniq' do
        let(:params)          { patch_one 'index_en.html', 'index_en_bad_continue_element_not_uniq.html' }
        let(:expected_errors) { ['The continue element with id="continue" must be uniq. Found 2 matches.'] }
        include_examples :wrong
      end
    end

    context 'index.js' do
      context 'Not found in right place' do
        let(:params)          { patch_one 'index_en.html', 'index_en_bad_js_not_found.html' }
        let(:expected_errors) { ['Plug index.js script before closing tag of body.'] }
        include_examples :wrong
      end
    end

    context 'index.css' do
      context 'Not found in right place' do
        let(:params)          { patch_one 'index_en.html', 'index_en_bad_css_not_found.html' }
        let(:expected_errors) { ['Plug index.css before closing tag of head.'] }
        include_examples :wrong
      end
    end

    context 'Some <a href="...">...</a> found' do
      let(:params) { patch_one 'index_en.html', 'index_en_bad_link_found.html' }
      let(:expected_errors) { ['Links with href attribute found.'] }
      include_examples :wrong
    end

    context 'favicon not found' do
      let(:params) { patch_one 'index_en.html', 'index_en_bad_favicon_not_found.html' }
      let(:expected_errors) { ['`link href="icon.ico"` not found.'] }
      include_examples :wrong
    end
  end

  def patch_one(key, file)
    good_params.dup.tap do |p|
      p[:files_content][key] = l(file)
    end
  end
end
