# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PrelandsRails::CreateSimpleSource::ValidateZipContent::ValidateIncomingFiles do
  subject { described_class.call({ incoming_files: incoming_files }) }

  context 'Nominal' do
    let(:incoming_files) do
      [
        { ftype: :file, name: 'index.html' },
        { ftype: :file, name: 'index.js' },
        { ftype: :file, name: 'index.css' },
        { ftype: :file, name: 'icon.ico' },
      ].map(&:to_struct)
    end

    it :works do
      expect([subject.success?, subject.errors]).to eq [true, nil]
    end
  end

  context 'Failed' do
    context 'App.js is not in root of zip-archive' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'js/index.js' },
          { ftype: :file, name: 'index.css' },
          { ftype: :file, name: 'icon.ico' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'The «index.js» JAVASCRIPT file is not in root of the zip-archive: expected «index.js», got «js/index.js»']
      end
    end
    context 'App.js is not found' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.css' },
          { ftype: :file, name: 'icon.ico' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'The «index.js» JAVASCRIPT file is not found in the zip-archive']
      end
    end
    context 'index.css is not in root of zip-archive' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.js' },
          { ftype: :file, name: 'css/index.css' },
          { ftype: :file, name: 'icon.ico' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'The «index.css» STYLES file is not in root of the zip-archive: expected «index.css», got «css/index.css»']
      end
    end
    context 'index.css is not found' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.js' },
          { ftype: :file, name: 'icon.ico' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'The «index.css» STYLES file is not found in the zip-archive']
      end
    end
    context 'favicon is not found' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.js' },
          { ftype: :file, name: 'index.css' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'favicon file is not found in the zip-archive']
      end
    end
    context 'favicon is not in root of zip-archive' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.js' },
          { ftype: :file, name: 'index.css' },
          { ftype: :file, name: 'img/icon.ico' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'favicon file is not in root of the zip-archive: got «img/icon.ico»']
      end
    end
    context '.svg found' do
      let(:incoming_files) do
        [
          { ftype: :file, name: 'index.html' },
          { ftype: :file, name: 'index.js' },
          { ftype: :file, name: 'index.css' },
          { ftype: :file, name: 'icon.ico' },
          { ftype: :file, name: 'img/image.svg' },
        ].map(&:to_struct)
      end

      it 'Returns validation error' do
        expect([subject.success?, subject.errors]).to eq [false, 'svg files detected, move inside css']
      end
    end
  end
end
