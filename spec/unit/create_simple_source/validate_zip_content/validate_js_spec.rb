require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::ValidateZipContent::ValidateJs do
  subject { described_class.call params }

  shared_examples :success do
    it(:works) { expect([subject.success?, subject.errors, subject.warnings]).to eq [true, nil, []] }
  end

  shared_examples :wrong do
    it 'Fails and return relevant error' do
      expect([subject.success?, subject.errors, subject.warnings]).to eq [true, nil, expected_warnings]
    end
  end

  let(:good_params) do
    {
      files_content: {
        'index.js' => l('index.js'),
      }
    }
  end

  context 'Nominal - valid js' do
    let(:params) { good_params }
    include_examples :success
  end

  context 'Invalid js' do
    context 'Location statement has found' do
      let(:params)            { patch_one 'index.js', 'index_js_bad_location_found.js' }
      let(:expected_warnings) { ['Location statement has found'] }
      include_examples :wrong
    end

    context 'Framework function\'s call not found' do
      let(:params)            { patch_one 'index.js', 'index_js_bad_fmwk_call_not_found.js' }
      let(:expected_warnings) { ['Location statement has found', 'Call of post_gender() not found'] }
      include_examples :wrong
    end
  end

  def patch_one(key, file)
    good_params.dup.tap do |p|
      p[:files_content][key] = l(file)
    end
  end
end
