require 'rails_helper'

RSpec.describe ::PrelandsRails::CreateSimpleSource::ValidateZipContent do
  subject { described_class.call params }

  shared_examples :works do
    it(:works) { expect([subject.success?, subject.errors]).to eq [true, nil] }
  end

end
