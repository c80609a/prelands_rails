# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::RecompileSimpleSource::DownloadZip do
  subject { described_class.call({ prelanding: prelanding }) }

  let(:archive_url) { 'https://s3.eu-central-1.amazonaws.com/pspzips/0bba88c2/src.zip' }

  let(:prelanding) do
    archive = double 'archive'
    expect(archive).to receive(:url).once.and_return archive_url

    source = double 'source'
    expect(source).to receive(:archive).once.and_return archive

    result = double 'prelanding'
    expect(result).to receive(:preland_simple_source).once.and_return source

    result
  end

  before do
    expect(Kernel).to receive(:open).with(archive_url).and_return('stream')
    expect(IO).to receive(:copy_stream).with('stream', Regexp.new('tmp'))
  end

  it :works do
    expect([subject.success?, subject.errors]).to eq [true, nil]
  end

end