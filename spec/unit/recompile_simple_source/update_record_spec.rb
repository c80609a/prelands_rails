require 'rails_helper'

# doubles
module UpdateRecordSpec
  class PrelandSimpleSource
    attr_reader :aws_prefix
    def initialize(params = {})
      @aws_prefix = params[:aws_prefix]
    end
    def self.find_by(*)
      new
    end
    def aws_prefix=(value)
      @aws_prefix = value
    end
    def save!; end
  end
end

RSpec.describe ::PrelandsRails::RecompileSimpleSource::UpdateRecord do
  subject { described_class.call params }

  let(:model_preland_simple_source) { ::UpdateRecordSpec::PrelandSimpleSource }

  shared_examples :success do
    before do
      expect_any_instance_of(model_preland_simple_source).to receive(:aws_prefix=).with(params[:aws_prefix])
      expect_any_instance_of(model_preland_simple_source).to receive(:save!)
      expect(model_preland_simple_source).to receive(:find_by).with({aws_prefix: params[:old_aws_prefix]}).and_call_original
    end
    it(:works) { expect([subject.success?, subject.errors]).to eq([true, nil]) }
  end

  let(:params) do
    {
      old_aws_prefix:              'abcd',
      aws_prefix:                  'new_abcd',
      model_preland_simple_source: model_preland_simple_source
    }
  end

  context 'Update' do
    let!(:preland_simple_source) do
      ::UpdateRecordSpec::PrelandSimpleSource.new({
        aws_prefix: params[:aws_prefix]
      })
    end
    include_examples :success
  end
end
