# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::RecompileSimpleSource::Upload do
  subject { described_class.call(params) }

  let(:params) do
    {
      compiled_htmls: {
        'index_ru.html' => '1',
        'index_en.html' => '2'
      },
      s3_credentials: { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
      bucket_names:   ['pstestbucket', 'date4more.eu'],
      aws_prefix:     '12312312',
      archive:        z('nominal.zip'),
      files_content:  { 'index.js' => 'alert("Hello, world")' }
    }
  end

  before do
    result = double 'result'
    expect(result).to receive(:success?).once.and_return(true)
    expect(::PrelandsRails::CreateSimpleSource::Upload).to receive(:call).once.and_return(result)
    expect_any_instance_of(described_class).to receive(:delete_compiled_files).once
  end

  it :works do
    expect([subject.success?, subject.errors]).to eq [true, nil]
  end
end