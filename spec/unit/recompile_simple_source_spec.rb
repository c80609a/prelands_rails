# frozen_string_literal: true

require 'rails_helper'

# doubles
module RecompileSimpleSourceSpec
  class PrelandSimpleSource
    attr_reader :aws_prefix
    def initialize(params = {})
      @aws_prefix = params[:aws_prefix]
    end
    def self.find_by(*)
      new
    end
    def archive=(value)
      @archive = value
    end
    def aws_prefix=(value)
      @aws_prefix = value
    end
    def save!; end
  end
end

RSpec.describe ::PrelandsRails::RecompileSimpleSource do
  subject { described_class.call params }

  let(:params) do
    {
      prelanding:                  create(:preland).tap { |p| create(:preland_simple_source, preland: p) },
      static_js_path:              'https://qwerty.s3.eu-central-1.amazonaws.com/static.js',
      s3_credentials:              { access_key: 'access_key', secret_key: 'secret_key', region: 'region' },
      bucket_names:                ['date4more.eu'],
      model_preland_simple_source: ::RecompileSimpleSourceSpec::PrelandSimpleSource,
      zip_bucket_name:             'pspzips'
    }
  end

  before do
    # каждый mocked интерактор пишет в контекст эти данные
    mock = Proc.new do |context|
      context.expected_locales = ['ru']
      context.aws_prefix       = ''
      context.archive          = z('nominal.zip')
      context.tmp_dest         = ''
      context.incoming_locales = ['ru']
      context.files_content    = {}
      context.compiled_htmls   = {}
      context.is_zip_moved     = true
    end

    [
      ::PrelandsRails::RecompileSimpleSource::DownloadZip,
      ::PrelandsRails::CreateSimpleSource::DetectIncomingLocales,
      ::PrelandsRails::CreateSimpleSource::CheckZipFiles,
      ::PrelandsRails::CreateSimpleSource::Compile,
      ::PrelandsRails::RecompileSimpleSource::Upload,
      ::PrelandsRails::UpdateSimpleSource::RemoveContentFromOldAwsPrefix,
      ::PrelandsRails::RecompileSimpleSource::RenameZipFile
    ].each do |i|
      expect(i).to receive(:call!).once &mock
    end
  end

  before do
    expect_any_instance_of(described_class).to receive(:clear_tmp_dest).once
  end

  before do
    expect(params[:prelanding].preland_simple_source).to receive(:update_columns).once.with({ updated_at: kind_of(Time) })
  end

  it :works do
    expect([subject.success?, subject.errors]).to eq [true, nil]
  end
end