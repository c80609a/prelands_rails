# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::UpdateSimpleSource::CheckZipFiles do
  subject { described_class.call params }

  context 'If archive present' do
    let(:params) do
      {
        archive:          z('nominal.zip'),
        expected_locales: ['ru'],
        incoming_locales: ['ru','en']
      }
    end

    context 'success' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(true)
        expect(result).to receive(:files_content).once.and_return({})
        expect(::PrelandsRails::CreateSimpleSource::CheckZipFiles).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          true,
          nil
        ])
      end
    end

    context 'fail' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(false)
        expect(result).not_to receive(:files_content)
        expect(result).to receive(:errors).and_return({some:'error'})
        expect(::PrelandsRails::CreateSimpleSource::CheckZipFiles).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          false,
          {some:'error'}
        ])
      end
    end
  end

  context 'If archive not present' do
    let(:params) do
      {
        expected_locales: ['ru'],
        incoming_locales: ['ru','en']
      }
    end

    before do
      expect(::PrelandsRails::CreateSimpleSource::CheckZipFiles).not_to receive(:call)
    end

    it :works do
      expect([
        subject.success?,
        subject.errors
      ]).to eq([
        false,
        'archive is missing'
      ])
    end
  end
end
