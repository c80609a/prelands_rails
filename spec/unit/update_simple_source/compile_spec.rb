require 'rails_helper'

RSpec.describe ::PrelandsRails::UpdateSimpleSource::Compile do
  subject { described_class.call params }

  context 'If files_content present' do
    let(:params) do
      {
        files_content:  { file_name: 'file_content' },
        static_js_path: 'https://static.js'
      }
    end

    context 'success' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(true)
        expect(result).to receive(:compiled_htmls).once.and_return({})
        expect(::PrelandsRails::CreateSimpleSource::Compile).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          true,
          nil
        ])
      end
    end

    context 'fail' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(false)
        expect(result).not_to receive(:compiled_htmls)
        expect(result).to receive(:errors).and_return({some:'error'})
        expect(::PrelandsRails::CreateSimpleSource::Compile).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          false,
          {some:'error'}
        ])
      end
    end
  end

  context 'If files_content not present' do
    let(:params) do
      {
        static_js_path: 'https://static.js'
      }
    end

    context 'success' do
      before do
        expect(::PrelandsRails::CreateSimpleSource::Compile).not_to receive(:call)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          true,
          nil
        ])
      end
    end
  end
end
