# frozen_string_literal: true

RSpec.describe ::PrelandsRails::UpdateSimpleSource::GenerateNewAwsPrefix, focus: 'generate_new_aws_prefix_spec' do
  subject { described_class.call params }

  context 'nominal' do
    let(:params) {{ aws_prefix: 'the_old_one' }}
    it '' do
      expect([subject.success?, subject.errors]).to eq [true, nil]
      expect(subject.aws_prefix).not_to eq params[:aws_prefix]
      expect(subject.old_aws_prefix).to be_truthy
    end
  end
end