# frozen_string_literal: true

RSpec.describe ::PrelandsRails::UpdateSimpleSource::RemoveContentFromOldAwsPrefix do
  subject { described_class.call params }

  context 'nominal' do
    let(:params) do
      {
        old_aws_prefix: 'old1234',
        s3_credentials: { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
        bucket_names:   ['date4more.eu']
      }
    end

    # файлы, которые нужно удалить
    let(:that_files)  {['%s/images/logo.png', '%s/index_en.html'].map { |path| path % params[:old_aws_prefix] }}

    # файлы, которые не нужно удалять
    let(:other_files) {['21112dcf/images/bg.png', 'dirty/static/js/1.736ca3bb.chunk.js']}

    # содержимое букета
    let(:all_files)   { that_files + other_files }

    # файлы, которые нужно удалить, как аргумент метода `delete_objects()`
    let(:deleting_files) do
      that_files.map do |file_name|
        { key: file_name }
      end
    end

    before do
      # expect api calls
      params[:bucket_names].each do |bname|
        expect_any_instance_of(::PrelandsRails::MyAwsClient).to receive(:list_objects).with(bname).once.and_return(all_files)
        expect_any_instance_of(::PrelandsRails::MyAwsClient).to receive(:delete_objects).with(bname, deleting_files).once
      end
    end

    it :works do
      expect([subject.success?, subject.errors]).to eq([true, nil])
    end
  end
end