# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PrelandsRails::RecompileSimpleSource::RenameZipFile do
  subject { described_class.call params }

  context 'nominal' do
    let(:params) do
      {
        aws_prefix:      'xxxxnew',
        old_aws_prefix:  'old1234',
        s3_credentials:  { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
        zip_bucket_name: 'pspzips'
      }
    end

    # expect api calls
    before do
      old_name = '%s/src.zip' % params[:old_aws_prefix]
      new_name = '%s/src.zip' % params[:aws_prefix]

      expect_any_instance_of(::PrelandsRails::MyAwsClient).to receive(:move_file).with(old_name, new_name, params[:zip_bucket_name]).once.and_return(true)
    end

    it :works do
      expect([subject.success?, subject.errors]).to eq([true, nil])
    end
  end
end