require 'rails_helper'

# doubles
module UpdateRecordSpec
  class PrelandSimpleSource
    attr_reader :aws_prefix, :archive, :preland_id, :preland_domain_ids, :locales
    def initialize(params = {})
      @aws_prefix         = params[:aws_prefix]
      @archive            = params[:archive]
      @preland_id         = params[:preland_id]
      @preland_domain_ids = params[:preland_domain_ids]
      @locales            = params[:locales]
    end
    def self.find_by(*)
      new
    end
    def archive=(value)
      @archive = value
    end
    def preland_domain_ids=(value)
      @preland_domain_ids = value
    end
    def locales=(value)
      @locale = value
    end
    def aws_prefix=(value)
      @aws_prefix = value
    end
    def save!; end
  end

  class Preland
    def id
      2
    end
  end

  class Domain
    attr_reader :url
    def initialize(url)
      @url = url
    end
    def id
      1
    end
  end
end

RSpec.describe ::PrelandsRails::UpdateSimpleSource::UpdateRecord do
  subject { described_class.call params }

  let(:model_preland_simple_source) { ::UpdateRecordSpec::PrelandSimpleSource }

  shared_examples :success do
    before do
      expect_any_instance_of(model_preland_simple_source).to receive(:locales=).with(params[:incoming_locales])
      expect_any_instance_of(model_preland_simple_source).to receive(:preland_domain_ids=).with(params[:preland_domain_ids])
      expect_any_instance_of(model_preland_simple_source).to receive(:archive=).with(params[:archive])
      expect_any_instance_of(model_preland_simple_source).to receive(:aws_prefix=).with(params[:aws_prefix])
      expect_any_instance_of(model_preland_simple_source).to receive(:save!)
      expect(model_preland_simple_source).to receive(:find_by).with({aws_prefix: params[:old_aws_prefix]}).and_call_original
    end
    it(:works) { expect([subject.success?, subject.errors]).to eq([true, nil]) }
  end

  let(:params) do
    {
      archive:                     z('nominal.zip'),
      old_aws_prefix:              'abcd',
      aws_prefix:                  'new_abcd',
      preland_id:                  preland,
      preland_domain_ids:          [domain.id],
      incoming_locales:            ['en', 'ru'],
      model_preland_simple_source: model_preland_simple_source
    }
  end

  let!(:preland) { ::UpdateRecordSpec::Preland.new }
  let!(:domain)  { ::UpdateRecordSpec::Domain.new 'https://date4more.eu' }
  let!(:domain2) { ::UpdateRecordSpec::Domain.new 'https://thea159.info' }

  context 'Update' do
    let!(:preland_simple_source) do
      ::UpdateRecordSpec::PrelandSimpleSource.new({
        archive:            z('nominal_ru.zip'),
        aws_prefix:         params[:aws_prefix],
        preland_id:         preland.id,
        preland_domain_ids: [domain2.id],
        locales:            ['ru']
      })
    end
    include_examples :success
  end
end
