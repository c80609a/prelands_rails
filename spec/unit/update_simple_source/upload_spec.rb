# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::UpdateSimpleSource::Upload do
  subject { described_class.call params }

  context 'If archive present' do
    let(:params) do
      {
        archive:          'nominal.zip',
        compiled_htmls:   { file_name: 'compiled content' },
        s3_credentials:   { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
        bucket_names:     ['name1', 'name2'],
        aws_prefix:       'prefix',
        files_content:    { file_name: 'file content' }
      }
    end

    before do
      expect(::PrelandsRails::CreateSimpleSource::Upload).to receive(:call).once
      expect_any_instance_of(described_class).to receive(:delete_compiled_files).once
    end

    it :works do
      expect([
        subject.success?,
        subject.errors
      ]).to eq([
        true,
        nil
      ])
    end
  end

  context 'If archive not present' do
    let(:params) do
      {
        s3_credentials: { access_key: 'access_key', secret_key: 'secret_key', region: 'region'},
        bucket_names:   ['name1', 'name2'],
        aws_prefix:     'prefix'
      }
    end

    before do
      expect(::PrelandsRails::CreateSimpleSource::Upload).not_to receive(:call)
      expect_any_instance_of(described_class).not_to receive(:delete_compiled_files)
    end

    it :works do
      expect([
        subject.success?,
        subject.errors
      ]).to eq([
        false,
        'archive is missing'
      ])
    end
  end
end
