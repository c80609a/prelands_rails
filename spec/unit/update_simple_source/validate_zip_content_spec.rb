# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::PrelandsRails::UpdateSimpleSource::ValidateZipContent do
  subject { described_class.call params }

  context 'If files_content present' do
    let(:params) do
      {
        files_content: { file_name: 'file content' }
      }
    end

    context 'success' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(true)
        expect(result).not_to receive(:errors)
        expect(result).to receive(:warnings).and_return([])
        expect(::PrelandsRails::CreateSimpleSource::ValidateZipContent).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          true,
          nil
        ])
      end
    end

    context 'fail' do
      before do
        result = double 'result'
        expect(result).to receive(:success?).once.and_return(false)
        expect(result).to receive(:errors).and_return({some:'error'})
        expect(::PrelandsRails::CreateSimpleSource::ValidateZipContent).to receive(:call).once.and_return(result)
      end

      it :works do
        expect([
          subject.success?,
          subject.errors
        ]).to eq([
          false,
          {some:'error'}
        ])
      end
    end
  end

  context 'If files_content not present' do
    let(:params) {{}}

    before do
      expect(::PrelandsRails::CreateSimpleSource::ValidateZipContent).not_to receive(:call)
    end

    it :works do
      expect([
        subject.success?,
        subject.errors
      ]).to eq([
        true,
        nil
      ])
    end
  end
end
